import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!');
  });

  it('/local-file/:uniqueId (GET)', () => {
    return request(app.getHttpServer()).get('/local-file/any').expect(404);
  });

  it('/local-folder/all (GET)', () => {
    return request(app.getHttpServer())
      .get('/local-folder/all')
      .expect(200)
      .expect(Array);
  });

  it('/local-folder/:serverRelativeUrl (GET)', () => {
    return request(app.getHttpServer())
      .get('/local-folder/myserverrelativeurl')
      .expect(404);
  });
});
