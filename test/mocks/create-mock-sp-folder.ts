import { SpFolder } from 'sharepoint-api-wrapper/dist/interfaces/microsoft/sp-folder.interface';
import { getRandomInt } from '../../src/utils/get-random-int';

export const createMockSpFolder = (options?: {
  uniqueId?: string;
  name?: string;
}): SpFolder => {
  if (!options) options = {};
  if (!options.uniqueId) options.uniqueId = 'test';
  if (!options.name) options.name = 'test';
  const timeCreated = Date.now().toString() + getRandomInt(1000);
  return {
    Files: {},
    ListItemAllFields: {},
    ParentFolder: {},
    Properties: {},
    Folders: {},
    ItemCount: 0,
    Name: options.name,
    ServerRelativeUrl: `/sites/testing/${options.name}`,
    WelcomePage: '',
    __metadata: {},
    StorageMetrics: {},
    Exists: true,
    IsWOPIEnabled: true,
    ProgID: null,
    TimeCreated: timeCreated,
    TimeLastModified: timeCreated,
    UniqueId: options.uniqueId,
  };
};
