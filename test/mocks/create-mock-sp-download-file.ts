import { SpDownloadFile } from 'sharepoint-api-wrapper/dist/interfaces/sp-download-file.interface';
import * as fs from 'fs';
import { join } from 'path';

const video = fs.readFileSync(join(__dirname, './mock-video.mp4'));

export const createMockSpDownloadFile = (): SpDownloadFile => {
  return {
    fileName: 'any',
    buffer: video,
  };
};
