import { SpFile } from 'sharepoint-api-wrapper/dist/interfaces/microsoft/sp-file.interface';

export const createMockSpFile = (
  uniqueId: string,
  extension: string,
  name?: string,
): SpFile => {
  if (!name) name = 'any';
  return {
    __metadata: {},
    Author: {},
    CheckedOutByUser: {},
    EffectiveInformationRightsManagementSettings: {},
    InformationRightsManagementSettings: {},
    ListItemAllFields: {},
    LockedByUser: {},
    ModifiedBy: {},
    Properties: {},
    VersionEvents: {},
    Versions: {},
    CheckInComment: 'string',
    CheckOutType: 0,
    ContentTag: 'string',
    CustomizedPageStatus: 0,
    ETag: 'string',
    Exists: true,
    IrmEnabled: true,
    Length: 'string',
    Level: 0,
    LinkingUri: 'string',
    LinkingUrl: 'string',
    MajorVersion: 0,
    MinorVersion: 0,
    Name: `${name}.${extension}`,
    ServerRelativeUrl: 'string',
    TimeCreated: 'string',
    TimeLastModified: 'string',
    Title: 'string',
    UIVersion: 0,
    UIVersionLabel: 0,
    UniqueId: uniqueId,
  };
};
