import * as Joi from 'joi';

export const ConfigSchema = Joi.object({
  // NestJS
  PORT: Joi.number().required().default(3000),
  // Sharepoint
  SHAREPOINT_URL: Joi.string().required(),
  SHAREPOINT_SITE_NAME: Joi.string().required(),
  SHAREPOINT_CLIENT_ID: Joi.string().required(),
  SHAREPOINT_CLIENT_SECRET: Joi.string().required(),
  SHAREPOINT_TENANT_ID: Joi.string().required(),
  FOLDER_OF_IMPORTS: Joi.string().required(),
  FOLDER_OF_ARCHIVES: Joi.string().required(),
  SHAREPOINT_SYNC_INTERVAL: Joi.number().required(),
  DOWNLOAD_INTERVAL: Joi.number().required(),
  DOWNLOAD_THREADS: Joi.number().required(),
  DELETE_FILES_INTERVAL: Joi.number().required(),
  MINUTES_S3_FILES_ALIVE: Joi.number().required(),
  // SQLite
  SQLITE_NAME: Joi.string().required(),
  // AWS S3
  S3_REGION: Joi.string().required(),
  S3_ACCESS_KEY: Joi.string().optional(),
  S3_ACCESS_SECRET: Joi.string().optional(),
  S3_BUCKET_NAME: Joi.string().required(),
  // OMEKA MySQL
  OMEKA_MYSQL_URL: Joi.string().required(),
});
