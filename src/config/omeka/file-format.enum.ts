// If need to add a FileFormat, just append the enum
export enum FileFormat {
  VIDEO = 'video',
  AUDIO = 'audio',
  EBOOK = 'ebook',
}
