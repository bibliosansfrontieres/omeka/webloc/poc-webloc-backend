import { FileFormat } from './file-format.enum';

/* 
This array is used to convert a file extension into a FileFormat
We can add a format in file-format.enum.ts and add here an ExtensionDefinition
We can add extension to a format, just append extensions array
 */
export const fileExtensionsConversions: ExtensionDefinition[] = [
  { format: FileFormat.AUDIO, extensions: ['mp3', 'wma', 'MP3', 'WMA'] },
  { format: FileFormat.VIDEO, extensions: ['mp4', 'mov', 'MP4', 'MOV'] },
  {
    format: FileFormat.EBOOK,
    extensions: [
      'pdf',
      'PDF',
      'docx',
      'DOCX',
      'epub',
      'EPUB',
      'gif',
      'GIF',
      'jpeg',
      'JPEG',
      'jpg',
      'JPG',
      'png',
      'PNG',
      'zip',
      'ZIP',
    ],
  },
];

interface ExtensionDefinition {
  format: FileFormat;
  extensions: string[];
}
