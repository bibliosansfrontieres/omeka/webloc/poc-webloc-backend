export enum ItemType {
  TEXT = 'Text',
  SOUND = 'Sound',
  MOVING_IMAGE = 'Moving Image',
}
