import { FileFormat } from './file-format.enum';
import { ItemType } from './item-type.enum';

// If need to add extensions to a type, just append the "extensions" array
export const fileFormatConversions: FormatDefinition[] = [
  { type: ItemType.SOUND, format: FileFormat.AUDIO },
  { type: ItemType.TEXT, format: FileFormat.EBOOK },
  { type: ItemType.MOVING_IMAGE, format: FileFormat.VIDEO },
];

interface FormatDefinition {
  type: ItemType;
  format: FileFormat;
}
