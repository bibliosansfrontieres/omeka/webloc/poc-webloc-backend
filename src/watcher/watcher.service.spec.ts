import { ConfigService } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';
import { Test, TestingModule } from '@nestjs/testing';
import { LocalFileService } from '../local-file/local-file.service';
import { LocalFolderService } from '../local-folder/local-folder.service';
import { SharepointService } from '../sharepoint/sharepoint.service';
import { WatcherService } from './watcher.service';

describe('WatcherService', () => {
  let service: WatcherService;

  beforeEach(async () => {
    const mockSharepointService = {
      getFolders: () => [],
    };
    const mockLocalFileService = {
      findAllToDownload: () => [],
    };
    const mockLocalFolderService = {
      findAll: () => [],
      findAllChanged: () => [],
    };

    const module: TestingModule = await Test.createTestingModule({
      imports: [ScheduleModule.forRoot()],
      providers: [
        WatcherService,
        SharepointService,
        LocalFileService,
        LocalFolderService,
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn((key: string) => {
              return key;
            }),
          },
        },
      ],
    })
      .overrideProvider(SharepointService)
      .useValue(mockSharepointService)
      .overrideProvider(LocalFileService)
      .useValue(mockLocalFileService)
      .overrideProvider(LocalFolderService)
      .useValue(mockLocalFolderService)
      .compile();

    service = module.get<WatcherService>(WatcherService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    service.schedulerRegistry.deleteInterval('checking-sharepoint');
  });
});
