import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { SchedulerRegistry } from '@nestjs/schedule';
import { SpFile } from 'sharepoint-api-wrapper/dist/interfaces/microsoft/sp-file.interface';
import { SpFolder } from 'sharepoint-api-wrapper/dist/interfaces/microsoft/sp-folder.interface';
import { LocalFileService } from '../local-file/local-file.service';
import { LocalFolder } from '../local-folder/entities/local-folder.entity';
import { LocalFolderService } from '../local-folder/local-folder.service';
import { SharepointService } from '../sharepoint/sharepoint.service';
import { isFileCompatible } from '../utils/is-file-compatible';
import * as fs from 'fs';
import { join } from 'path';
import { LocalFile } from 'src/local-file/entities/local-file.entity';
import { SocketGateway } from 'src/socket/socket.gateway';
import { AwsS3Service } from 'src/aws-s3/aws-s3.service';

/**
 * WatcherService
 * @property {boolean} checking - Is already checking Sharepoint site ?
 */
@Injectable()
export class WatcherService {
  private isCheckingFolders: boolean;
  private isCheckingDownloads: boolean;
  private isDeletingFiles: boolean;
  private downloadThreads: number;
  private minutesS3FilesAlive: number;

  constructor(
    private configService: ConfigService,
    public schedulerRegistry: SchedulerRegistry,
    private sharepointService: SharepointService,
    private localFileService: LocalFileService,
    private localFolderService: LocalFolderService,
    private awsS3Service: AwsS3Service,
    private socketGateway: SocketGateway,
  ) {
    this.minutesS3FilesAlive = this.configService.get<number>(
      'MINUTES_S3_FILES_ALIVE',
    );
    this.downloadThreads = this.configService.get<number>('DOWNLOAD_THREADS');
    this.isCheckingFolders = false;
    this.isCheckingDownloads = false;
    this.addCheckingSharepointInterval();
  }

  /**
   * Add a checkSharepoint interval
   * Used to intervally check Sharepoint changes
   * @returns {void}
   */
  private addCheckingSharepointInterval(): void {
    const checkFilesInterval = this.configService.get<number>(
      'SHAREPOINT_SYNC_INTERVAL',
    );
    const checkFoldersInterval = setInterval(
      this.doCheckFolders.bind(this),
      checkFilesInterval,
    );
    this.schedulerRegistry.addInterval(
      'checking-folders',
      checkFoldersInterval,
    );
    const downloadInterval =
      this.configService.get<number>('DOWNLOAD_INTERVAL');
    const checkDownloadsInterval = setInterval(
      this.doCheckDownloads.bind(this),
      downloadInterval,
    );
    this.schedulerRegistry.addInterval(
      'checking-downloads',
      checkDownloadsInterval,
    );
    const deleteInterval = this.configService.get<number>(
      'DELETE_FILES_INTERVAL',
    );
    const checkToDeleteInterval = setInterval(
      this.doDeleteFiles.bind(this),
      deleteInterval,
    );
    this.schedulerRegistry.addInterval('deleting-files', checkToDeleteInterval);
  }

  /**
   * Check Sharepoint folders and files
   * Only check if no other check is running
   * @returns {Promise<void>}
   */
  private async doCheckFolders(): Promise<void> {
    if (this.isCheckingFolders) return;
    this.isCheckingFolders = true;
    await this.checkFolders();
    await this.checkFiles();
    this.isCheckingFolders = false;
  }

  /**
   * Check downloads
   * Only check if no other check is running
   * @returns {Promise<void>}
   */
  private async doCheckDownloads(): Promise<void> {
    if (this.isCheckingDownloads) return;
    this.isCheckingDownloads = true;
    const toDownload = await this.localFileService.findAllToDownload();
    const downloadThreads = [];
    for (let i = 0; i < this.downloadThreads; i++) {
      downloadThreads.push(this.checkDownloads(toDownload, i));
    }
    await Promise.all(downloadThreads);
    this.isCheckingDownloads = false;
  }

  /**
   * Check downloads
   * Only check if no other check is running
   * @returns {Promise<void>}
   */
  private async doDeleteFiles(): Promise<void> {
    if (this.isDeletingFiles) return;
    this.isDeletingFiles = true;
    await this.localFileService.deleteAskedDeletion(
      1000 * 60 * this.minutesS3FilesAlive,
    );
    this.isDeletingFiles = false;
  }

  /**
   * Check if folders lastModifiedTime has changed
   * Mark folders that has been changed
   * @returns {Promise<void>}
   */
  private async checkFolders(): Promise<void> {
    const spFolders = await this.sharepointService.getFolders();
    if (!spFolders) return;
    const localFolders = await this.localFolderService.findAll();
    await this.updateLocalFoldersFromSharepoint(localFolders, spFolders);
    await this.removeDeletedSharepointFolders(localFolders, spFolders);
  }

  /**
   * Check if files has been changed in folders that has been changed
   * For each folder that has been marked as changed
   */
  private async checkFiles(): Promise<void> {
    const localFolders = await this.localFolderService.findAllChanged();

    for (const localFolder of localFolders) {
      const spFiles = await this.sharepointService.getFiles(
        localFolder.serverRelativeUrl,
      );
      if (!spFiles) return;
      await this.removeDeletedSharepointFiles(localFolder, spFiles);
      const isFolderNeedRecheck = await this.updateFilesFromSharepoint(
        localFolder,
        spFiles,
      );
      if (!isFolderNeedRecheck) {
        await this.localFolderService.markUpdated(localFolder);
      }
    }
  }

  /**
   * Check if some LocalFiles have to be downloaded
   * Download files and write them locally
   * @returns {Promise<void>}
   */
  private async checkDownloads(
    localFiles: LocalFile[],
    id: number,
  ): Promise<void> {
    while (localFiles.length > 0) {
      const localFile = localFiles.shift();
      if (localFile.length !== '0') {
        Logger.log(
          `Thread [${id}] - Downloading ${localFile.name} in ${localFile.localFolder.name}`,
          'WatcherService',
        );
        const filePath = join(
          __dirname,
          `../../downloads/${localFile.uniqueId}`,
        );
        const stream = fs.createWriteStream(filePath);
        const isDownloaded = await this.sharepointService.download(
          localFile,
          stream,
        );
        if (isDownloaded) {
          await this.localFileService.getMetadata(localFile);
          continue;
        } else {
          if (fs.existsSync(filePath)) {
            fs.rmSync(filePath);
          }
          Logger.error(
            `Thread [${id}] - Could not download ${localFile.name} in ${localFile.localFolder.name}`,
            'WatcherService',
          );
        }
      }
    }
  }

  /**
   * Update LocalFolders from SpFolders
   * @param {LocalFolder[]} localFolders - LocalFolders actually present in database
   * @param {SpFolder[]} spFolders - SpFolders actually present in Sharepoint
   * @returns {Promise<void>}
   */
  private async updateLocalFoldersFromSharepoint(
    localFolders: LocalFolder[],
    spFolders: SpFolder[],
  ): Promise<void> {
    for (const spFolder of spFolders) {
      const foundLocalFolder = localFolders.find((localFolder) => {
        return localFolder.uniqueId === spFolder.UniqueId;
      });
      if (!foundLocalFolder) {
        await this.localFolderService.create(spFolder);
        Logger.log(`Found new folder "${spFolder.Name}"`, 'WatcherService');
      } else if (
        foundLocalFolder.timeLastModified !== spFolder.TimeLastModified
      ) {
        await this.localFolderService.changeDetected(
          foundLocalFolder,
          spFolder,
        );
        Logger.log(
          `Need to update folder "${spFolder.Name}"`,
          'WatcherService',
        );
      }
    }
  }

  /**
   * Remove LocalFolders deleted from Sharepoint
   * @param {LocalFolder[]} localFolders - LocalFolders actually present in database
   * @param {SpFolder[]} spFolders - SpFolders actually present in Sharepoint
   * @returns {Promise<void>}
   */
  private async removeDeletedSharepointFolders(
    localFolders: LocalFolder[],
    spFolders: SpFolder[],
  ): Promise<void> {
    for (const localFolder of localFolders) {
      const foundFolder = spFolders.find(
        (spFolder) => spFolder.UniqueId === localFolder.uniqueId,
      );
      if (!foundFolder && !localFolder.isExported) {
        for (const localFile of localFolder.localFiles) {
          await this.localFileService.delete(localFile);
          await this.awsS3Service.deleteObject(localFile.awsKey);
        }
        await this.localFolderService.delete(localFolder);
        Logger.log(`Removed folder "${localFolder.name}"`, 'WatcherService');
      }
    }
  }

  /**
   * Update LocalFiles from SpFiles
   * @param {LocalFolder} localFolder - LocalFolder where to update LocalFiles
   * @param {SpFile[]} spFiles - All SpFiles that are actually in Sharepoint
   * @returns {Promise<void>}
   */
  private async updateFilesFromSharepoint(
    localFolder: LocalFolder,
    spFiles: SpFile[],
  ): Promise<boolean> {
    let isFolderNeedRecheck = false;

    const localFiles = await this.localFileService.findAllByLocalFolder(
      localFolder,
    );

    for (const spFile of spFiles) {
      const foundLocalFile = localFiles.find(
        (localFile) => localFile.uniqueId === spFile.UniqueId,
      );

      if (isFileCompatible(spFile.Name)) {
        if (spFile.Length === '0') {
          isFolderNeedRecheck = true;
        }
      }

      if (!foundLocalFile) {
        if (isFileCompatible(spFile.Name)) {
          await this.localFileService.create(spFile, localFolder);
          Logger.log(
            `Found new file "${spFile.Name}" in folder "${localFolder.name}"`,
            'WatcherService',
          );
        } else {
          await this.localFileService.create(spFile, localFolder, false);
          Logger.log(
            `File found "${spFile.Name}" in "${localFolder.name}" not compatible with Omeka, ignored`,
            'WatcherService',
          );
        }
      } else if (
        (foundLocalFile.timeLastModified !== spFile.TimeLastModified ||
          foundLocalFile.length !== spFile.Length) &&
        foundLocalFile.compatible
      ) {
        if (spFile.Length === '0') {
          if (!foundLocalFile.uploading) {
            await this.localFileService.changeDetected(foundLocalFile, spFile);
            Logger.log(
              `Found uploading or corrupted file "${spFile.Name}" in folder ${localFolder.name}`,
              'WatcherService',
            );
          }
          isFolderNeedRecheck = true;
        } else {
          await this.localFileService.changeDetected(foundLocalFile, spFile);
          Logger.log(
            `Found changes on file "${spFile.Name}" in folder ${localFolder.name}`,
            'WatcherService',
          );
        }
      }
    }

    return isFolderNeedRecheck;
  }

  /**
   * Remove all deleted files from Sharepoint
   * @param {LocalFolder} localFolder - LocalFolder where to delete files
   * @param {SpFile[]} spFiles - List of SpFiles that are actually on Sharepoint folder
   * @returns {Promise<void>}
   */
  private async removeDeletedSharepointFiles(
    localFolder: LocalFolder,
    spFiles: SpFile[],
  ): Promise<void> {
    if (!localFolder.isExported) {
      const localFiles = await this.localFileService.findAllByLocalFolder(
        localFolder,
      );
      for (const localFile of localFiles) {
        const foundSpFile = spFiles.find(
          (spFile) => spFile.UniqueId === localFile.uniqueId,
        );
        if (!foundSpFile) {
          await this.localFileService.delete(localFile);
          await this.awsS3Service.deleteObject(localFile.awsKey);
          this.socketGateway.emitRemoveLocalFile(localFolder.id, localFile);
          Logger.log(
            `Removed file "${localFile.name}" in folder "${localFolder.name}"`,
            'WatcherService',
          );
        }
      }
    }
  }
}
