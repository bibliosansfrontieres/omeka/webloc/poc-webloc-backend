import { Module } from '@nestjs/common';
import { WatcherService } from './watcher.service';
import { SharepointModule } from '../sharepoint/sharepoint.module';
import { LocalFileModule } from '../local-file/local-file.module';
import { LocalFolderModule } from '../local-folder/local-folder.module';
import { SocketModule } from 'src/socket/socket.module';
import { AwsS3Module } from 'src/aws-s3/aws-s3.module';

@Module({
  imports: [
    SharepointModule,
    LocalFileModule,
    LocalFolderModule,
    SocketModule,
    AwsS3Module,
  ],
  providers: [WatcherService],
})
export class WatcherModule {}
