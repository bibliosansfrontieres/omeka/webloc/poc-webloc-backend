export interface FolderStatus {
  status: 'ready' | 'not-ready';
}
