import {
  MessageBody,
  OnGatewayConnection,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { FolderStatus } from './interfaces/folder-status.interface';
import { LocalFile } from 'src/local-file/entities/local-file.entity';
import { LocalFolderService } from 'src/local-folder/local-folder.service';
import { Inject, forwardRef } from '@nestjs/common';
import { LocalFileService } from 'src/local-file/local-file.service';

@WebSocketGateway({ cors: true })
export class SocketGateway implements OnGatewayConnection {
  @WebSocketServer()
  private server: Server;

  constructor(
    @Inject(forwardRef(() => LocalFolderService))
    private localFolderService: LocalFolderService,
    private localFileService: LocalFileService,
  ) {}

  handleConnection(client: Socket) {}

  @SubscribeMessage('getInformations')
  handleGetInformationsEvent(@MessageBody() data: { id: number }) {
    this.emitInformations(data.id);
  }

  async emitInformations(id: number) {
    const localFolder = await this.localFolderService.findById(id);
    if (!localFolder) return;
    const localFiles = await this.localFileService.findAllByLocalFolder(
      localFolder,
    );
    const informations = {
      status: localFolder.changeDetected ? 'not-ready' : 'ready',
      localFiles: localFiles.map((localFile) => ({
        id: localFile.id,
        status: localFile.ready ? 'ready' : 'not-ready',
      })),
    };
    this.server.emit(`localFolderInformations#${id}`, informations, localFiles);
  }

  emitRemoveLocalFile(id: number, localFile: LocalFile) {
    this.server.emit(`localFileRemove#${id}`, localFile);
  }
}
