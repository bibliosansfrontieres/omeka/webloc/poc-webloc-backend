import { Module, forwardRef } from '@nestjs/common';
import { SocketGateway } from './socket.gateway';
import { LocalFolderModule } from 'src/local-folder/local-folder.module';
import { LocalFileModule } from 'src/local-file/local-file.module';

@Module({
  imports: [forwardRef(() => LocalFolderModule), LocalFileModule],
  providers: [SocketGateway],
  exports: [SocketGateway],
})
export class SocketModule {}
