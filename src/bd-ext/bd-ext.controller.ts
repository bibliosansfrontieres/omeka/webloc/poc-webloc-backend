import {
  Controller,
  Get,
  HttpCode,
  NotFoundException,
  Param,
} from '@nestjs/common';
import { BdExtService } from './bd-ext.service';

@Controller('bd-ext')
export class BdExtController {
  constructor(private readonly bdExtService: BdExtService) {}

  @HttpCode(200)
  @Get('languages')
  getLanguages() {
    return this.bdExtService.getLanguages();
  }

  @Get('licenses')
  getLicences() {
    const licenses = this.bdExtService.getLicenses();
    return { licenses };
  }

  @Get('full/:iso')
  get(@Param('iso') iso: string) {
    const bdExt = this.bdExtService.getBdExt(iso);
    if (!bdExt) throw new NotFoundException();
    return bdExt;
  }

  @Get('all')
  getAll() {
    return this.bdExtService.getAll();
  }

  @Get('audiences/:iso')
  @HttpCode(200)
  getAudiences(@Param('iso') iso: string) {
    const audiences = this.bdExtService.getAudiences(iso);
    if (!audiences) throw new NotFoundException();
    return {
      audiences,
    };
  }
}
