import { Injectable, Logger } from '@nestjs/common';
import * as fs from 'fs';
import { join } from 'path';
import { Audience } from './interfaces/audience.interface';
import { BdExt } from './interfaces/bd-ext.interface';
import { JsonLanguagesFile } from './interfaces/json-languages-file.interface';
import { Language } from './interfaces/language.interface';
import { LanguageList } from './interfaces/language-list.interface';
import { License } from './interfaces/license.interface';

@Injectable()
export class BdExtService {
  bdExtDirPath: string = join(__dirname, '../../bd-ext-sources');
  bdExts: Map<string, BdExt>;
  licences: License[];
  audiences: Map<string, string[]>;
  languages: LanguageList<Language>;

  constructor() {
    this.bdExts = new Map();
    this.licences = [];
    this.audiences = new Map();
    this.loadBdExt();
    this.loadLicenses();
    this.loadLanguages();
    this.loadAudiences();
  }

  loadBdExt() {
    const filesList = fs.readdirSync(this.bdExtDirPath);
    const filteredFiles = filesList.filter((fileName) =>
      fileName.endsWith('.json'),
    );
    const fileNames = filteredFiles.map((fileName) =>
      fileName.replace('.json', ''),
    );
    for (const fileName of fileNames) {
      const bdExt = this.loadFileToJson<BdExt>(fileName, true);
      this.bdExts.set(bdExt.iso, bdExt);
      Logger.log(`Loaded [${bdExt.iso}] bdExt file`, 'BdExtService');
    }
  }

  loadLicenses() {
    const licenses = this.loadFileToJson<{ licenses: License[] }>('licenses');
    for (const license of licenses.licenses) {
      this.licences.push(license);
    }
    Logger.log(`Loaded ${licenses.licenses.length} licenses`, 'BdExtService');
  }

  loadLanguages() {
    this.languages = this.loadFileToJson<LanguageList<Language>>('languages');
    Logger.log(`Loaded languages`, 'BdExtService');
  }

  loadAudiences() {
    const audiences =
      this.loadFileToJson<JsonLanguagesFile<Audience>>('audiences');
    for (const audience of audiences.languages) {
      this.audiences.set(audience.iso, audience.audiences);
      Logger.log(`Loaded [${audience.iso}] audiences`, 'BdExtService');
    }
  }

  loadFileToJson<T>(fileName: string, isBdExt = false): T {
    const otherDir = isBdExt ? '/' : '/other';
    const filePath = join(
      __dirname,
      `../../bd-ext-sources${otherDir}/${fileName}.json`,
    );
    const file = fs.readFileSync(filePath);
    const jsonContent = JSON.parse(file.toString()) as T;
    return jsonContent;
  }

  getBdExt(iso: string): BdExt | undefined {
    const bdExt = this.bdExts.get(iso);
    if (!bdExt) return undefined;
    return bdExt;
  }

  getAll(): BdExt[] {
    const bdExts: BdExt[] = [];
    for (let [key, value] of this.bdExts) {
      bdExts.push(value);
    }
    return bdExts;
  }

  getLicenses(): License[] {
    return this.licences;
  }

  getAudiences(iso: string): string[] | undefined {
    const audiences = this.audiences.get(iso);
    if (!audiences) return undefined;
    return audiences;
  }

  getLanguages(): LanguageList<Language> {
    return this.languages;
  }
}
