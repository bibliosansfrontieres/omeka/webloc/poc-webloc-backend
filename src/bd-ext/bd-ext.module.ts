import { Module } from '@nestjs/common';
import { BdExtController } from './bd-ext.controller';
import { BdExtService } from './bd-ext.service';

@Module({
  imports: [],
  controllers: [BdExtController],
  providers: [BdExtService],
  exports: [BdExtService],
})
export class BdExtModule {}
