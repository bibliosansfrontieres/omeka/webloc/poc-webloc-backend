export interface License {
  name: string;
  title: string;
}
