export interface Audience {
  iso: string;
  audiences: string[];
}
