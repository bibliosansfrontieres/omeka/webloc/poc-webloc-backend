import { JsonLanguagesFile } from './json-languages-file.interface';

export interface LanguageList<T> extends JsonLanguagesFile<T> {
  languages: T[];
  special_languages: T[];
}
