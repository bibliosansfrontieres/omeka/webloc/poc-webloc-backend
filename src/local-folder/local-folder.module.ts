import { forwardRef, Module } from '@nestjs/common';
import { LocalFolderService } from './local-folder.service';
import { LocalFolderController } from './local-folder.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LocalFolder } from './entities/local-folder.entity';
import { SharepointModule } from 'src/sharepoint/sharepoint.module';
import { SaveModule } from 'src/save/save.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([LocalFolder], 'WebLoc'),
    SharepointModule,
    forwardRef(() => SaveModule),
  ],
  controllers: [LocalFolderController],
  providers: [LocalFolderService],
  exports: [LocalFolderService],
})
export class LocalFolderModule {}
