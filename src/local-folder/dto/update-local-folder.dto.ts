import { PartialType } from '@nestjs/mapped-types';
import { CreateLocalFolderDto } from './create-local-folder.dto';

export class UpdateLocalFolderDto extends PartialType(CreateLocalFolderDto) {}
