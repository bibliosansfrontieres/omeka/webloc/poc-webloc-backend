import { forwardRef, Inject, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { join } from 'path';
import * as fs from 'fs';
import { SpFolder } from 'sharepoint-api-wrapper/dist/interfaces/microsoft/sp-folder.interface';
import { Repository } from 'typeorm';
import { LocalFolder } from './entities/local-folder.entity';
import { SharepointService } from 'src/sharepoint/sharepoint.service';
import { CreateSaveDto } from 'src/save/dto/create-save.dto';
import { SaveService } from 'src/save/save.service';

@Injectable()
export class LocalFolderService {
  private readonly downloadsPath = '../../downloads';

  constructor(
    @InjectRepository(LocalFolder, 'WebLoc')
    private localFolderRepository: Repository<LocalFolder>,
    private sharepointService: SharepointService,
    @Inject(forwardRef(() => SaveService)) private saveService: SaveService,
  ) {}

  /* istanbul ignore next */
  onModuleInit() {
    const downloadsDirPath = join(__dirname, this.downloadsPath);
    const isDownloadDirExists = () => fs.existsSync(downloadsDirPath);
    if (isDownloadDirExists()) {
      Logger.log('Downloads directory found.', 'LocalStorageService');
    } else {
      fs.mkdirSync(downloadsDirPath);
      if (isDownloadDirExists()) {
        Logger.log('Downloads directory created.', 'LocalStorageService');
      } else {
        Logger.error(
          'Downloads directory creation failed',
          'LocalStorageService',
        );
        throw new Error(
          'Error while creating downloads directory ! Please create it manually',
        );
      }
    }
  }

  /**
   * Create a LocalFolder from a SpFolder
   * @param {SpFolder} spFolder - SpFolder used to create LocalFolder
   */
  async create(spFolder: SpFolder): Promise<void> {
    const localFolder = new LocalFolder(spFolder);
    await this.localFolderRepository.save(localFolder);
  }

  /**
   * Delete a LocalFolder
   * @param {LocalFolder} localFolder - LocalFolder to delete
   */
  async delete(localFolder: LocalFolder): Promise<void> {
    try {
      await this.localFolderRepository.remove(localFolder);
    } catch (e) {
      console.error(`could not delete folder ${localFolder.id}`);
      console.error(e);
    }
  }

  /**
   * Mark a folder as changes detected
   * This will update timeLastModified and serverRelativeUrl
   * @param {LocalFolder} localFolder - LocalFolder to mark changes detected
   * @param {SpFolder} spFolder - New folder from Sharepoint that has changes
   */
  async changeDetected(
    localFolder: LocalFolder,
    spFolder: SpFolder,
  ): Promise<LocalFolder> {
    localFolder.changeDetected = true;
    localFolder.timeLastModified = spFolder.TimeLastModified;
    localFolder.serverRelativeUrl = spFolder.ServerRelativeUrl;
    localFolder.name = spFolder.Name;
    return this.localFolderRepository.save(localFolder);
  }

  /**
   * Mark a folder as updated
   * This will switch changeDetected to false
   * @param localFolder - LocalFolder to mark as updated
   */
  async markUpdated(localFolder: LocalFolder): Promise<LocalFolder> {
    localFolder.changeDetected = false;
    return this.localFolderRepository.save(localFolder);
  }

  /**
   * Find all LocalFolders
   * @returns {Promise<LocalFolder[]>}
   */
  /* istanbul ignore next */
  findAll(isExported?: boolean): Promise<LocalFolder[]> {
    return this.localFolderRepository.find({
      where: { isExported },
      relations: ['localFiles'],
    });
  }

  /**
   * Find all LocalFolders that have been marked as changed
   * @returns {Promise<LocalFolder[]>}
   */
  /* istanbul ignore next */
  findAllChanged(): Promise<LocalFolder[]> {
    return this.localFolderRepository.find({ where: { changeDetected: true } });
  }

  /**
   * Find a LocalFolder by it's database ID
   * @param {number} id - LocalFolder ID
   * @returns {Promise<LocalFolder | undefined>}
   */
  /* istanbul ignore next */
  async findById(id: number, email?: string): Promise<LocalFolder | undefined> {
    const relations = [];
    if (email) {
      relations.push('localFiles', 'saves');
    }
    const localFolder = await this.localFolderRepository.findOne({
      where: { id },
      relations,
      order:
        relations.length > 0
          ? {
              localFiles: {
                id: 'ASC',
              },
              saves: {
                timestamp: 'DESC',
              },
            }
          : undefined,
    });
    if (!localFolder) return undefined;
    if (email && localFolder.saves) {
      localFolder.saves = localFolder.saves.filter(
        (save) => save.email === null || save.email === email,
      );
    }
    return localFolder;
  }

  async export(id: number, createSaveDto: CreateSaveDto) {
    let localFolder = await this.localFolderRepository.findOne({
      where: { id },
    });
    if (!localFolder) return undefined;
    // Create an export save
    const save = await this.saveService.create(
      createSaveDto,
      localFolder,
      true,
    );
    if (!save) return undefined;
    await this.saveService.removeNotExportedSaves(localFolder);
    localFolder.isExported = true;
    localFolder = await this.localFolderRepository.save(localFolder);
    await this.sharepointService.moveFolder(localFolder);
    return localFolder;
  }

  async canExport(id: number) {
    let canExport = true;
    const localFolder = await this.findById(id, 'canexport');
    if (!localFolder || localFolder.changeDetected) return false;
    for (const localFile of localFolder.localFiles) {
      if (!localFile.ready) {
        canExport = false;
        break;
      }
    }
    return canExport;
  }
}
