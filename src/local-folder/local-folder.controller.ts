import {
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Post,
  Query,
} from '@nestjs/common';
import { CreateSaveDto } from 'src/save/dto/create-save.dto';
import { LocalFolderService } from './local-folder.service';

@Controller('local-folder')
export class LocalFolderController {
  constructor(private readonly localFolderService: LocalFolderService) {}

  @Get('all')
  getFolders() {
    return this.localFolderService.findAll(false);
  }

  @Get(':id')
  async getFolder(@Param('id') id: string, @Query('email') email: string) {
    const localFolder = await this.localFolderService.findById(+id, email);
    if (!localFolder) throw new NotFoundException();
    return localFolder;
  }

  @Post('export/:id')
  async postExport(
    @Param('id') id: string,
    @Body() createSaveDto: CreateSaveDto,
  ) {
    const localFolder = await this.localFolderService.export(
      +id,
      createSaveDto,
    );
    if (!localFolder) throw new NotFoundException();
    return localFolder;
  }

  @Get('canExport/:id')
  async getCanExport(@Param('id') id: string) {
    return {
      canExport: await this.localFolderService.canExport(+id),
    };
  }
}
