import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { createMockSpFolder } from '../../test/mocks/create-mock-sp-folder';
import { Repository } from 'typeorm';
import { LocalFolder } from './entities/local-folder.entity';
import { LocalFolderService } from './local-folder.service';
import * as fs from 'fs';
import { join } from 'path';
import { createMockSpFile } from '../../test/mocks/create-mock-sp-file';
import { LocalFile } from '../local-file/entities/local-file.entity';

describe('LocalFolderService', () => {
  let service: LocalFolderService;
  let localFolderRepository: Repository<LocalFolder>;
  const LOCAL_FOLDER_REPOSITORY_TOKEN = getRepositoryToken(
    LocalFolder,
    'WebLoc',
  );
  const downloadsPath = join(__dirname, '../../downloads');

  // Sharepoint mocks
  const mockSpFile = createMockSpFile('test', 'mp4');
  const mockSpFolder = createMockSpFolder({ uniqueId: 'should-not-exists' });
  const mockChangedSpFolder = createMockSpFolder({
    name: 'my name',
    uniqueId: 'should-not-exists',
  });

  // Local mocks
  const mockLocalFolder = new LocalFolder(mockSpFolder);
  const mockLocalFile = new LocalFile(mockSpFile, mockLocalFolder);

  // Relations between Local mocks
  mockLocalFolder.localFiles = [mockLocalFile];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        LocalFolderService,
        {
          provide: LOCAL_FOLDER_REPOSITORY_TOKEN,
          useClass: Repository,
        },
      ],
    }).compile();

    service = module.get<LocalFolderService>(LocalFolderService);
    localFolderRepository = module.get<Repository<LocalFolder>>(
      LOCAL_FOLDER_REPOSITORY_TOKEN,
    );

    // save will return the same object with an ID
    jest
      .spyOn(localFolderRepository, 'save')
      .mockImplementation((localFolder) => {
        return new Promise((resolve) => {
          resolve({ id: Date.now(), ...localFolder } as LocalFolder);
        });
      });

    // remove will return void
    jest.spyOn(localFolderRepository, 'remove').mockResolvedValue(void 0);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('localFolderRepository should be defined', () => {
    expect(localFolderRepository).toBeDefined();
  });

  describe('create', () => {
    it('should create a folder wich name is unique ID', async () => {
      await service.create(mockSpFolder);
      const folderPath = `${downloadsPath}/${mockSpFolder.UniqueId}`;
      const exists = fs.existsSync(folderPath);
      expect(exists).toBe(true);
    });
  });

  describe('delete', () => {
    it('should delete an existing folder', async () => {
      await service.delete(mockLocalFolder);
      const folderPath = `${downloadsPath}/${mockLocalFolder.uniqueId}`;
      const exists = fs.existsSync(folderPath);
      expect(exists).toBe(false);
    });
  });

  describe('changeDetected', () => {
    it('should mark a folder as changes detected', async () => {
      const localFolder = await service.changeDetected(
        mockLocalFolder,
        mockChangedSpFolder,
      );
      expect(localFolder.name).toBe(mockChangedSpFolder.Name);
      expect(localFolder.serverRelativeUrl).not.toBe(
        mockSpFolder.ServerRelativeUrl,
      );
      expect(localFolder.timeLastModified).not.toBe(
        mockSpFolder.TimeLastModified,
      );
      expect(localFolder.changeDetected).toBe(true);
    });
  });

  describe('markUpdated', () => {
    it('should mark a folder as updated', async () => {
      const localFolder = await service.markUpdated(mockLocalFolder);
      expect(localFolder.changeDetected).toBe(false);
    });
  });
});
