import { Test, TestingModule } from '@nestjs/testing';
import { LocalFile } from '../local-file/entities/local-file.entity';
import { createMockSpFile } from '../../test/mocks/create-mock-sp-file';
import { createMockSpFolder } from '../../test/mocks/create-mock-sp-folder';
import { LocalFolder } from './entities/local-folder.entity';
import { LocalFolderController } from './local-folder.controller';
import { LocalFolderService } from './local-folder.service';
import { NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';

describe('LocalFolderController', () => {
  let controller: LocalFolderController;
  let localFolderRepository: Repository<LocalFolder>;
  const LOCAL_FOLDER_REPOSITORY_TOKEN = getRepositoryToken(
    LocalFolder,
    'WebLoc',
  );

  // Sharepoint mocks
  const mockSpFolder = createMockSpFolder();
  const mockSpFileAudio = createMockSpFile('test', 'mp3');

  // Local mocks
  const mockLocalFolder = new LocalFolder(mockSpFolder);
  const mockLocalFile = new LocalFile(mockSpFileAudio, mockLocalFolder);
  const mockReadyLocalFolder = { ...mockLocalFolder };
  const mockReadyLocalFile = { ...mockLocalFile };
  mockReadyLocalFolder.changeDetected = false;
  mockReadyLocalFile.ready = true;
  mockReadyLocalFile.changeDetected = false;

  // Relations between Local mocks
  mockLocalFolder.localFiles = [mockLocalFile];
  mockReadyLocalFolder.localFiles = [mockReadyLocalFile];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LocalFolderController],
      providers: [
        LocalFolderService,
        {
          provide: LOCAL_FOLDER_REPOSITORY_TOKEN,
          useClass: Repository,
        },
      ],
    }).compile();

    controller = module.get<LocalFolderController>(LocalFolderController);
    localFolderRepository = module.get<Repository<LocalFolder>>(
      LOCAL_FOLDER_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('/:folderRelativeUrl', () => {
    it('should find a LocalFolder by its serverRelativeUrl', async () => {
      // findOne will return mockLocalFolder
      jest
        .spyOn(localFolderRepository, 'findOne')
        .mockResolvedValueOnce(mockLocalFolder);

      const localFolder = await controller.getFolder(`/sites/any/any`);
      expect(localFolder).toBeInstanceOf(LocalFolder);
    });

    it('should throw NotFoundException if LocalFolder not found', async () => {
      // findOne will return null
      jest.spyOn(localFolderRepository, 'findOne').mockResolvedValueOnce(null);

      const localFolder = controller.getFolder(`/sites/any/any`);
      await expect(localFolder).rejects.toThrowError(NotFoundException);
    });
  });
});
