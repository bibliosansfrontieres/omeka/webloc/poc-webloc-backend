import { SpFolder } from 'sharepoint-api-wrapper/dist/interfaces/microsoft/sp-folder.interface';
import { LocalFile } from '../../local-file/entities/local-file.entity';
import { Save } from '../../save/entities/save.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class LocalFolder {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  changeDetected: boolean;

  @Column()
  isExported: boolean;

  @OneToMany(() => LocalFile, (localFile) => localFile.localFolder)
  localFiles: LocalFile[];

  @OneToMany(() => Save, (save) => save.localFolder)
  saves: Save[];

  /* Sharepoint data */

  @Column()
  name: string;

  @Column()
  uniqueId: string;

  @Column()
  timeLastModified: string;

  @Column()
  serverRelativeUrl: string;

  constructor(spFolder: SpFolder) {
    if (!spFolder) return;
    this.changeDetected = true;
    this.isExported = false;

    /* Sharepoint data */
    this.name = spFolder.Name;
    this.uniqueId = spFolder.UniqueId;
    this.timeLastModified = spFolder.TimeLastModified;
    this.serverRelativeUrl = spFolder.ServerRelativeUrl;
  }
}
