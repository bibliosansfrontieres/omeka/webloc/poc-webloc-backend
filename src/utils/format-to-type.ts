import { fileFormatConversions } from '../config/omeka/file-format-conversion';
import { FileFormat } from '../config/omeka/file-format.enum';
import { ItemType } from '../config/omeka/item-type.enum';

export function formatToType(format: FileFormat): ItemType | undefined {
  let type: ItemType | undefined = undefined;
  for (const conversion of fileFormatConversions) {
    if (conversion.format === format) {
      type = conversion.type;
    }
  }
  return type;
}
