import { ItemType } from '../config/omeka/item-type.enum';
import { FileFormat } from '../config/omeka/file-format.enum';
import { extensionToFormat } from './extension-to-format';
import { formatToType } from './format-to-type';
import { getFileExtension } from './get-file-extension';
import { getRandomInt } from './get-random-int';
import { isFileCompatible } from './is-file-compatible';

describe('Utils', () => {
  describe('getFileExtension', () => {
    it('should return the extension of the file', () => {
      const fileNames = ['file.mp3', 'file.myfile.mp4', 'mylastfile.pdf'];
      expect(getFileExtension(fileNames[0])).toBe('mp3');
      expect(getFileExtension(fileNames[1])).toBe('mp4');
      expect(getFileExtension(fileNames[2])).toBe('pdf');
    });
  });

  describe('extensionToFormat', () => {
    it('should return the format of the file', () => {
      const extensions = ['mp3', 'mp4', 'pdf'];
      expect(extensionToFormat(extensions[0])).toBe(FileFormat.AUDIO);
      expect(extensionToFormat(extensions[1])).toBe(FileFormat.VIDEO);
      expect(extensionToFormat(extensions[2])).toBe(FileFormat.EBOOK);
    });
  });

  describe('formatToType', () => {
    it('should return the item type of the file', () => {
      const fileFormats = [
        FileFormat.AUDIO,
        FileFormat.VIDEO,
        FileFormat.EBOOK,
      ];
      expect(formatToType(fileFormats[0])).toBe(ItemType.SOUND);
      expect(formatToType(fileFormats[1])).toBe(ItemType.MOVING_IMAGE);
      expect(formatToType(fileFormats[2])).toBe(ItemType.TEXT);
    });
  });

  describe('getRandomInt', () => {
    it('should return a random integer', () => {
      expect(true).toBe(true);
    });
  });

  describe('isFileCompatible', () => {
    it('should return true for MP3, MP4 and PDF', () => {
      expect(isFileCompatible('test.mp3')).toBe(true);
      expect(isFileCompatible('test.mp4')).toBe(true);
      expect(isFileCompatible('test.pdf')).toBe(true);
    });

    it('should return false for any other extension', () => {
      expect(isFileCompatible('test.stl')).toBe(false);
      expect(isFileCompatible('test.docx')).toBe(false);
    });
  });
});
