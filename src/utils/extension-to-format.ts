import { fileExtensionsConversions } from '../config/omeka/file-extension-conversion';
import { FileFormat } from '../config/omeka/file-format.enum';

export function extensionToFormat(extension: string): FileFormat | undefined {
  let format: FileFormat | undefined = undefined;
  for (const conversion of fileExtensionsConversions) {
    if (conversion.extensions.includes(extension)) {
      format = conversion.format;
    }
  }
  return format;
}
