import { extensionToFormat } from './extension-to-format';
import { formatToType } from './format-to-type';
import { getFileExtension } from './get-file-extension';

export function isFileCompatible(fileName: string): boolean {
  const extension = getFileExtension(fileName);
  const format = extensionToFormat(extension);
  if (!format) return false;
  return formatToType(format) !== undefined;
}
