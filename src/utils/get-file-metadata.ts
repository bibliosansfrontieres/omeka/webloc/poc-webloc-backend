import { FileFormat } from '../config/omeka/file-format.enum';
import { PdfData } from 'pdfdataextract';
import * as ffprobe from 'ffprobe-client';
import { Metadata } from '../local-file/interfaces/metadata.interface';
import * as moment from 'moment';
import * as fs from 'fs';
import { LocalFile } from 'src/local-file/entities/local-file.entity';
import { join } from 'path';
import { Logger } from '@nestjs/common';

export async function getFileMetadata(localFile: LocalFile): Promise<Metadata> {
  const metadata: Metadata = {};
  const filePath = join(__dirname, `../../downloads/${localFile.uniqueId}`);
  if (localFile.format === FileFormat.EBOOK && localFile.extension === 'pdf') {
    try {
      const data = fs.readFileSync(filePath);
      const pdfData = await PdfData.extract(data, { get: { pages: true } });
      if (pdfData.pages) metadata.size = `${pdfData.pages}`;
    } catch (e) {
      Logger.error(e, 'getFileMetadata');
    }
  } else if (
    localFile.format === FileFormat.AUDIO ||
    localFile.format === FileFormat.VIDEO
  ) {
    try {
      const videoInfos = await ffprobe(filePath);
      const duration = Math.round(videoInfos.format.duration);
      metadata.duration = moment.utc(duration * 1000).format('HH:mm:ss');
    } catch (e) {
      console.error(e);
      Logger.error(e, 'getFileMetadata');
    }
  }

  if (!metadata.size && localFile.format !== FileFormat.EBOOK) {
    metadata.size = (parseInt(localFile.length) / (1000 * 1000)).toFixed(2);
  }
  /*
  if (fs.existsSync(filePath)) {
    fs.rmSync(filePath);
  }
*/
  return metadata;
}
