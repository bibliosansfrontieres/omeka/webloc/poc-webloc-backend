export function getFileName(fileServerRelativeUrl: string): string {
  const fileNameSplit = fileServerRelativeUrl.split('/');
  return fileNameSplit[fileNameSplit.length - 1];
}
