import { Save } from 'src/save/entities/save.entity';

export function sortSaves(saves: Save[]): Save[] {
  return saves.sort((a, b) => {
    return parseInt(b.timestamp) - parseInt(a.timestamp);
  });
}
