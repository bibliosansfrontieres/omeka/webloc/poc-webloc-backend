import {
  Controller,
  Get,
  Logger,
  NotFoundException,
  Param,
  Res,
} from '@nestjs/common';
import { LocalFileService } from './local-file.service';
import { Response } from 'express';
import { AwsS3Service } from 'src/aws-s3/aws-s3.service';
import { Readable } from 'stream';

@Controller('local-file')
export class LocalFileController {
  constructor(
    private readonly localFileService: LocalFileService,
    private awsS3Service: AwsS3Service,
  ) {}

  @Get(':awsKey')
  async downloadFile(@Param('awsKey') awsKey: string, @Res() res: Response) {
    const localFile = await this.localFileService.findByAwsKey(awsKey);
    if (!localFile) throw new NotFoundException();

    const filename = localFile.name.replace(/[^0-9a-zA-Z,.]+/g, '');

    res.set({
      'Content-Disposition': `attachment; filename="${filename}"`,
    });

    const object = await this.awsS3Service.getObject(localFile.awsKey);
    if (!object.Body) throw new NotFoundException();

    const stream = object.Body as Readable;
    stream.pipe(res);

    stream.on('end', async () => {
      if (
        localFile.localFolder.isExported &&
        localFile.askedDeletion === null
      ) {
        await this.localFileService.askDeletion(localFile);
      }
    });
  }
}
