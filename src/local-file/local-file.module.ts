import { Module } from '@nestjs/common';
import { LocalFileService } from './local-file.service';
import { LocalFileController } from './local-file.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LocalFile } from './entities/local-file.entity';
import { AwsS3Module } from 'src/aws-s3/aws-s3.module';

@Module({
  imports: [TypeOrmModule.forFeature([LocalFile], 'WebLoc'), AwsS3Module],
  controllers: [LocalFileController],
  providers: [LocalFileService],
  exports: [LocalFileService],
})
export class LocalFileModule {}
