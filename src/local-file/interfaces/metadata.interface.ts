export interface Metadata {
  pages?: number;
  size?: string;
  duration?: string;
  audios?: number;
  subtitles?: number;
}
