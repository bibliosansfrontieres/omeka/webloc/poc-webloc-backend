import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { DeepPartial, Repository } from 'typeorm';
import * as fs from 'fs';
import { join } from 'path';

import { LocalFile } from './entities/local-file.entity';
import { LocalFileService } from './local-file.service';
import { LocalFolder } from '../local-folder/entities/local-folder.entity';

import { createMockSpFile } from '../../test/mocks/create-mock-sp-file';
import { createMockSpDownloadFile } from '../../test/mocks/create-mock-sp-download-file';
import { createMockSpFolder } from '../../test/mocks/create-mock-sp-folder';

describe('LocalFileService', () => {
  let service: LocalFileService;
  let localFileRepository: Repository<LocalFile>;
  const LOCAL_FILE_REPOSITORY_TOKEN = getRepositoryToken(LocalFile, 'WebLoc');
  const downloadsPath = join(__dirname, '../../downloads');

  // Sharepoint mocks
  const mockSpFileVideo = createMockSpFile('test', 'mp4');
  const mockChangedSpFileVideo = createMockSpFile('test', 'mp4', 'any2');
  const mockSpDownloadFile = createMockSpDownloadFile();

  // Local mocks
  const mockSpFolder = createMockSpFolder();
  const mockLocalFolder = new LocalFolder(mockSpFolder);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        LocalFileService,
        {
          provide: LOCAL_FILE_REPOSITORY_TOKEN,
          useClass: Repository,
        },
      ],
    }).compile();

    service = module.get<LocalFileService>(LocalFileService);
    localFileRepository = module.get<Repository<LocalFile>>(
      LOCAL_FILE_REPOSITORY_TOKEN,
    );

    // save will return the same object with an ID
    jest.spyOn(localFileRepository, 'save').mockImplementation((localFile) => {
      return new Promise((resolve) => {
        resolve({ id: Date.now(), ...localFile } as LocalFile);
      });
    });

    // remove will return void
    jest.spyOn(localFileRepository, 'remove').mockResolvedValue(void 0);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('localFileRepository should be defined', () => {
    expect(localFileRepository).toBeDefined();
  });

  describe('changeDetected', () => {
    it('should mark a LocalFile as changes detected', async () => {
      let localFile = new LocalFile(mockSpFileVideo, mockLocalFolder);
      expect(localFile.name).toBe(mockSpFileVideo.Name);

      localFile = await service.changeDetected(
        localFile,
        mockChangedSpFileVideo,
      );
      expect(localFile.name).toBe(mockChangedSpFileVideo.Name);
      expect(localFile.changeDetected).toBe(true);
      expect(localFile.ready).toBe(false);
    });
  });

  const videoLocalFile = new LocalFile(mockSpFileVideo, mockLocalFolder);

  describe('write', () => {
    it('should write a LocalFile locally in its folder', async () => {
      await service.write(videoLocalFile, mockSpDownloadFile);
      const exists = fs.existsSync(
        `${downloadsPath}/${videoLocalFile.localFolder.uniqueId}/${videoLocalFile.uniqueId}.${videoLocalFile.extension}`,
      );
      expect(exists).toBe(true);
    });
  });

  describe('delete', () => {
    it('should delete a LocalFile locally in its folder', async () => {
      await service.delete(videoLocalFile);
      const exists = fs.existsSync(
        `${downloadsPath}/${videoLocalFile.localFolder.uniqueId}/${videoLocalFile.uniqueId}.${videoLocalFile.extension}`,
      );
      expect(exists).toBe(false);
    });
  });
});
