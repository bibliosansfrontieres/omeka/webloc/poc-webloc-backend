import { SpFile } from 'sharepoint-api-wrapper/dist/interfaces/microsoft/sp-file.interface';
import { FileFormat } from '../../config/omeka/file-format.enum';
import { ItemType } from '../../config/omeka/item-type.enum';
import { LocalFolder } from '../../local-folder/entities/local-folder.entity';
import { FileMetadata } from '../../save/entities/file-metadata.entity';
import { extensionToFormat } from '../../utils/extension-to-format';
import { formatToType } from '../../utils/format-to-type';
import { getFileExtension } from '../../utils/get-file-extension';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class LocalFile {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  changeDetected: boolean;

  @Column()
  ready: boolean;

  @Column()
  uploading: boolean;

  @Column()
  compatible: boolean;

  @Column()
  awsKey: string;

  @ManyToOne(() => LocalFolder, (localFolder) => localFolder.localFiles, {
    onDelete: 'CASCADE',
  })
  localFolder: LocalFolder;

  @OneToMany(() => FileMetadata, (fileMetadata) => fileMetadata.localFile)
  fileMetadatas: FileMetadata[];

  /* Sharepoint data */

  @Column()
  uniqueId: string;

  @Column()
  name: string;

  @Column()
  timeLastModified: string;

  @Column()
  timeCreated: string;

  @Column()
  serverRelativeUrl: string;

  @Column()
  length: string;

  /* Metadata */

  @Column()
  extension: string;

  @Column()
  format: FileFormat;

  @Column()
  type: ItemType;

  @Column({ nullable: true })
  size: string;

  @Column({ nullable: true })
  duration: string;

  @Column({ nullable: true })
  askedDeletion: number;

  constructor(spFile: SpFile, localFolder: LocalFolder, compatible: boolean) {
    if (!spFile || !localFolder) return;

    this.compatible = compatible;

    if (spFile.Length === '0') {
      spFile.TimeLastModified = '0';
      this.uploading = true;
      this.changeDetected = false;
    } else {
      this.uploading = false;
      this.changeDetected = true;
    }

    this.ready = false;
    this.localFolder = localFolder;

    /* Sharepoint data */
    this.uniqueId = spFile.UniqueId;
    this.timeLastModified = spFile.TimeLastModified;
    this.timeCreated = spFile.TimeCreated;
    this.serverRelativeUrl = spFile.ServerRelativeUrl;
    this.name = spFile.Name;
    this.length = spFile.Length;

    /* Metadata */
    this.extension = getFileExtension(spFile.Name);
    this.format = extensionToFormat(this.extension) || FileFormat.AUDIO;
    this.type = formatToType(this.format) || ItemType.SOUND;

    this.awsKey = `${spFile.UniqueId}.${this.extension}`;
  }
}
