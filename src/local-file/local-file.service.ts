import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SpFile } from 'sharepoint-api-wrapper/dist/interfaces/microsoft/sp-file.interface';
import { LocalFolder } from '../local-folder/entities/local-folder.entity';
import { And, IsNull, LessThanOrEqual, Not, Repository } from 'typeorm';
import { LocalFile } from './entities/local-file.entity';
import { join } from 'path';
import * as fs from 'fs';
import { getFileMetadata } from '../utils/get-file-metadata';
import fetch from 'node-fetch';
import { AwsS3Service } from 'src/aws-s3/aws-s3.service';

@Injectable()
export class LocalFileService {
  public readonly downloadsPath = join(__dirname, '../../downloads');

  constructor(
    @InjectRepository(LocalFile, 'WebLoc')
    private localFileRepository: Repository<LocalFile>,
    private awsS3Service: AwsS3Service,
  ) {}

  /**
   * Create a local file from a SpFile and a LocalFolder
   * @param {SpFile} spFile - SpFile used to create local file
   * @param {LocalFolder} localFolder - Local folder where the local file will be located
   * @returns {Promise<LocalFile>}
   */
  /* istanbul ignore next */
  async create(
    spFile: SpFile,
    localFolder: LocalFolder,
    compatible: boolean = true,
  ): Promise<LocalFile> {
    const localFile = new LocalFile(spFile, localFolder, compatible);
    return this.localFileRepository.save(localFile);
  }

  /**
   * Find all files by folder unique ID
   * @param {string} uniqueId - Unique ID of folder where to find files
   * @returns {Promise<LocalFile[]>}
   */
  /* istanbul ignore next */
  async findAllByFolderUniqueId(uniqueId: string): Promise<LocalFile[]> {
    return this.localFileRepository.find({
      where: { uniqueId },
    });
  }

  /**
   * Find all LocalFiles from a LocalFolder
   * @param {LocalFolder} localFolder - LocalFolder where to find local files
   * @returns {Promise<LocalFile[]>}
   */
  /* istanbul ignore next */
  findAllByLocalFolder(localFolder: LocalFolder): Promise<LocalFile[]> {
    return this.localFileRepository.find({
      where: { localFolder },
      relations: { localFolder: true },
    });
  }

  /**
   * Find a LocalFile by UniqueId
   * @param {string} uniqueId - UniqueId of LocalFile to find
   * @returns {Promise<LocalFile | undefined>}
   */
  /* istanbul ignore next */
  async findByUniqueId(
    uniqueId: string,
    isExported?: boolean,
  ): Promise<LocalFile | undefined> {
    const file = await this.localFileRepository.findOne({
      where: {
        uniqueId,
        ready: true,
      },
      relations: ['localFolder', 'localFolder.localFiles'],
    });
    if (!file) return undefined;
    if (isExported && !file.localFolder.isExported) return undefined;
    return file;
  }

  /**
   * Find a LocalFile by AwsKey
   * @param {string} awsKey - AwsKey of LocalFile to find
   * @returns {Promise<LocalFile | undefined>}
   */
  /* istanbul ignore next */
  async findByAwsKey(awsKey: string): Promise<LocalFile | undefined> {
    const file = await this.localFileRepository.findOne({
      where: {
        awsKey,
        ready: true,
      },
      relations: ['localFolder', 'localFolder.localFiles'],
    });
    if (!file) return undefined;
    return file;
  }

  /**
   * Find a LocalFile by it's local database ID
   * @param {number} id - ID of LocalFile to find
   * @returns {Promise<LocalFile | undefined>}
   */
  /* istanbul ignore next */
  async findById(id: number): Promise<LocalFile | undefined> {
    const file = await this.localFileRepository.findOne({
      where: {
        id,
      },
    });
    if (!file) return undefined;
    return file;
  }

  /**
   * Find all LocalFiles to download
   * Files to downloads are files marked by changes detected
   * @returns {Promise<LocalFile[]>}
   */
  /* istanbul ignore next */
  async findAllToDownload(): Promise<LocalFile[]> {
    return this.localFileRepository.find({
      where: { changeDetected: true },
      relations: { localFolder: true },
    });
  }

  /**
   * Mark a file as changes detected
   * This will update timeLastModified and serverRelativeUrl
   * This will also update ready to false (we can't download this file for the moment)
   * @param {LocalFile} localFile - LocalFile to mark changes detected
   * @param {SpFile} spFile - New file from Sharepoint that has changes
   * @returns {Promise<LocalFile>}
   */
  async changeDetected(
    localFile: LocalFile,
    spFile: SpFile,
  ): Promise<LocalFile> {
    if (spFile.Length !== '0') {
      localFile.timeLastModified = spFile.TimeLastModified;
      localFile.changeDetected = true;
      localFile.uploading = false;
    } else {
      localFile.timeLastModified = '0';
      localFile.uploading = true;
    }
    localFile.ready = false;
    localFile.serverRelativeUrl = spFile.ServerRelativeUrl;
    localFile.name = spFile.Name;
    localFile.length = spFile.Length;
    return await this.localFileRepository.save(localFile);
  }

  /**
   * Write a LocalFile that has been downloaded from Sharepoint
   * @param {LocalFile} localFile - LocalFile that has been downloaded
   */
  write(localFile: LocalFile): fs.WriteStream {
    const folderPath = `${this.downloadsPath}/${localFile.localFolder.uniqueId}`;
    const filePath = `${this.downloadsPath}/${localFile.localFolder.uniqueId}/${localFile.uniqueId}.${localFile.extension}`;

    if (!fs.existsSync(folderPath)) fs.mkdirSync(folderPath);
    if (fs.existsSync(filePath)) fs.rmSync(filePath);

    return fs.createWriteStream(filePath);
  }

  async getMetadata(localFile: LocalFile) {
    const metadata = await getFileMetadata(localFile);

    localFile.duration = metadata.duration;
    localFile.size = metadata.size;
    localFile.changeDetected = false;
    localFile.ready = true;

    await this.localFileRepository.save(localFile);
  }

  /**
   * Delete a LocalFile
   * @param {LocalFile} localFile - LocalFile to delete
   */
  async delete(localFile: LocalFile): Promise<void> {
    try {
      await this.localFileRepository.remove(localFile);
    } catch (e) {
      console.error(`could not delete file ${localFile.id}`);
      console.error(e);
    }
  }

  async downloadFromAws(
    awsKey: string,
  ): Promise<
    { stream: NodeJS.ReadableStream; localFile: LocalFile } | undefined
  > {
    const localFile = await this.findByAwsKey(awsKey);
    if (!localFile) return undefined;
    const res = await fetch(`${this.awsS3Service.url}/${localFile.awsKey}`);
    if (res.ok) {
      return {
        stream: res.body,
        localFile,
      };
    }
    return undefined;
  }

  async askDeletion(localFile: LocalFile): Promise<void> {
    localFile.askedDeletion = Date.now();
    await this.localFileRepository.save(localFile);
  }

  async deleteAskedDeletion(msForDeletion: number) {
    const timeToDelete = Date.now() - msForDeletion;
    const localFiles = await this.localFileRepository.find({
      where: {
        askedDeletion: And(Not(IsNull()), LessThanOrEqual(timeToDelete)),
      },
    });
    for (const localFile of localFiles) {
      try {
        await this.delete(localFile);
        await this.awsS3Service.deleteObject(localFile.awsKey);
      } catch (e) {
        console.log('Could not delete file');
        console.error(e);
      }
    }
  }
}
