import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { NotFoundException, StreamableFile } from '@nestjs/common';
import { Repository } from 'typeorm';

import { LocalFileController } from './local-file.controller';
import { LocalFileService } from './local-file.service';
import { LocalFile } from './entities/local-file.entity';
import { LocalFolder } from '../local-folder/entities/local-folder.entity';

import { createMockSpFile } from '../../test/mocks/create-mock-sp-file';
import { createMockSpFolder } from '../../test/mocks/create-mock-sp-folder';

describe('LocalFileController', () => {
  let controller: LocalFileController;
  let localFileRepository: Repository<LocalFile>;
  const LOCAL_FILE_REPOSITORY_TOKEN = getRepositoryToken(LocalFile, 'WebLoc');

  // Sharepoint mocks
  const mockSpFolder = createMockSpFolder();
  const mockSpFileVideo = createMockSpFile('test-download', 'mp4');

  // Local mocks
  const mockLocalFolder = new LocalFolder(mockSpFolder);
  const mockLocalFile = new LocalFile(mockSpFileVideo, mockLocalFolder);

  // Relations between Local mocks
  const mockLocalFiles = [mockLocalFile];
  mockLocalFile.localFolder = mockLocalFolder;
  mockLocalFolder.localFiles = mockLocalFiles;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LocalFileController],
      providers: [
        LocalFileService,
        {
          provide: LOCAL_FILE_REPOSITORY_TOKEN,
          useClass: Repository,
        },
      ],
    }).compile();

    controller = module.get<LocalFileController>(LocalFileController);
    localFileRepository = module.get<Repository<LocalFile>>(
      LOCAL_FILE_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('localFileRepository should be defined', () => {
    expect(localFileRepository).toBeDefined();
  });

  describe('/:uniqueId', () => {
    it('should download a file', async () => {
      // findOne will return mockLocalFile
      jest
        .spyOn(localFileRepository, 'findOne')
        .mockResolvedValueOnce(mockLocalFile);

      const download = await controller.downloadFile('anyUniqueId');
      expect(download).toBeDefined();
      expect(download).toBeInstanceOf(StreamableFile);

      const headers = download.getHeaders();
      const fileName = `${mockLocalFile.uniqueId}.${mockLocalFile.extension}`;
      expect(headers.type).toBe('application/octet-stream');
      expect(headers.disposition).toBe(`attachment; filename="${fileName}"`);
      expect(headers.length).toBeGreaterThan(0);
    });

    it('should throw NotFoundException if LocalFile not found', async () => {
      // findOne will return null
      jest.spyOn(localFileRepository, 'findOne').mockResolvedValueOnce(null);

      const download = controller.downloadFile('anyUniqueId');
      await expect(download).rejects.toThrowError(NotFoundException);
    });
  });
});
