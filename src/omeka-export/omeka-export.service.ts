import { Injectable } from '@nestjs/common';
import { CreateOmekaExportDto } from './dto/create-omeka-export.dto';
import { OmekaExport } from './entities/omeka-export.entity';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { DataSource, LegacyOracleNamingStrategy, Repository } from 'typeorm';
import { stringify } from 'csv-stringify/sync';
import { PassThrough } from 'stream';
import { AwsS3Service } from 'src/aws-s3/aws-s3.service';
import { Chunk1 } from './interfaces/chunks/chunk1.interface';
import { Chunk2 } from './interfaces/chunks/chunk2.interface';
import { Chunk3 } from './interfaces/chunks/chunk3.interface';
import { Chunk4 } from './interfaces/chunks/chunk4.interface';
import { Chunk5 } from './interfaces/chunks/chunk5.interface';
import { Chunk6 } from './interfaces/chunks/chunk6.interface';
import { Chunk7 } from './interfaces/chunks/chunk7.interface';
import { Chunk8 } from './interfaces/chunks/chunk8.interface';
import { Chunk9 } from './interfaces/chunks/chunk9.interface';
import { Chunk10 } from './interfaces/chunks/chunk10.interface';
import { Chunk11 } from './interfaces/chunks/chunk11.interface';
import { Chunk12 } from './interfaces/chunks/chunk12.interface';
import { OmekaExportItemDto } from './dto/omeka-export-item.dto';
import { Interval } from '@nestjs/schedule';
import { AskedOmekaExport } from './interfaces/asked-omeka-export';
import { CreateOmekaExportPackageDto } from './dto/create-omeka-export-package.dto';
import { getColumns } from './interfaces/columns';
import { Chunk13 } from './interfaces/chunks/chunk13.interface';
import { Chunk14 } from './interfaces/chunks/chunk14.interface';
import { Chunk15 } from './interfaces/chunks/chunk15.interface';

@Injectable()
export class OmekaExportService {
  private askedOmekaExports: AskedOmekaExport[];
  private exportThreadRunning: boolean;

  constructor(
    @InjectDataSource('Omeka') private dataSource: DataSource,
    @InjectRepository(OmekaExport, 'OmekaExport')
    private omekaExportRepository: Repository<OmekaExport>,
    private awsS3Service: AwsS3Service,
  ) {
    this.askedOmekaExports = [];
  }

  @Interval(1000)
  async exportThread() {
    if (!this.exportThreadRunning) {
      this.exportThreadRunning = true;
      while (this.askedOmekaExports.length > 0) {
        const askedOmekaExport = this.askedOmekaExports.shift();
        await this.generateExport(askedOmekaExport);
      }
      this.exportThreadRunning = false;
    }
  }

  async getExports(
    page: number = 1,
  ): Promise<{ pages: number; page: number; exports: OmekaExport[] }> {
    const limit = 10;
    const pages = Math.ceil((await this.omekaExportRepository.count()) / limit);
    const skip = (page - 1) * limit;
    const exports = await this.omekaExportRepository.find({
      take: limit,
      skip,
      order: { id: 'DESC' },
    });
    return { page, pages, exports };
  }

  async getExport(id: number) {
    return this.omekaExportRepository.findOne({ where: { id } });
  }

  async getPackagesFromProjects(
    createOmekaExportDto: CreateOmekaExportDto,
  ): Promise<CreateOmekaExportPackageDto[]> {
    const projectIds = createOmekaExportDto.projects.map(
      (project) => +project.project_id,
    );
    const query = `
    SELECT package_relations.package_id AS package_id, package_relations.item_id AS project_id
    FROM omeka_package_manager_packages_relations as package_relations
    WHERE package_relations.item_id IN (${projectIds.join(',')});`;
    const result = await this.dataSource.query(query);
    return result.map((res: any) => ({
      package_id: res.package_id,
      project_id: res.project_id,
    }));
  }

  async askExport(
    createOmekaExportDto: CreateOmekaExportDto,
  ): Promise<{ id: number }> {
    if (createOmekaExportDto.packages.length === 0) {
      if (createOmekaExportDto.projects.length === 0) {
        return;
      }
      createOmekaExportDto.packages = await this.getPackagesFromProjects(
        createOmekaExportDto,
      );
    }
    if (createOmekaExportDto.packages.length !== 0) {
      let omekaExport = new OmekaExport(createOmekaExportDto);
      omekaExport = await this.omekaExportRepository.save(omekaExport);
      this.askedOmekaExports.push({ omekaExport, createOmekaExportDto });
      return { id: omekaExport.id };
    }
    return;
  }

  async askExportAllPackages(email: string) {
    const query = `
    SELECT package.id as package_id
    FROM omeka_package_manager_packages as package;`;
    const result = await this.dataSource.query(query);
    const createOmekaExportDto: CreateOmekaExportDto = {
      email,
      packages: result,
      projects: [],
    };
    let omekaExport = new OmekaExport(createOmekaExportDto);
    omekaExport = await this.omekaExportRepository.save(omekaExport);
    this.askedOmekaExports.push({ omekaExport, createOmekaExportDto });
    return { id: omekaExport.id };
  }

  async generateExport(askedOmekaExport: AskedOmekaExport) {
    const omekaExport = askedOmekaExport.omekaExport;
    const items: OmekaExportItemDto[] = [];
    if (askedOmekaExport.createOmekaExportDto.packages.length <= 10) {
      items.push(
        ...(await this.getItemsFromPackages(
          askedOmekaExport.createOmekaExportDto.packages,
        )),
      );
    } else {
      const total = askedOmekaExport.createOmekaExportDto.packages.length;
      let lastProgression: number | undefined = undefined;
      while (askedOmekaExport.createOmekaExportDto.packages.length > 0) {
        const getAskedPackages =
          askedOmekaExport.createOmekaExportDto.packages.splice(0, 10);
        const actual = askedOmekaExport.createOmekaExportDto.packages.length;
        const donePackages = total - actual;
        const purcentage = parseInt(((donePackages / total) * 100).toString());
        if (lastProgression !== purcentage) {
          lastProgression = purcentage;
          console.log(
            `Progression "${askedOmekaExport.omekaExport.id}": ` +
              parseInt(purcentage.toString()) +
              '%',
          );
        }
        items.push(...(await this.getItemsFromPackages(getAskedPackages)));
      }
      console.log(`Omeka export "${askedOmekaExport.omekaExport.id}" is done`);
    }

    const exportedItems: OmekaExportItemDto[] = items.map((item) => {
      const exportedItem = { ...item };
      for (const key of Object.keys(item.custom)) {
        exportedItem[`custom_${key}`] = item.custom[key];
      }
      delete exportedItem.custom;
      return exportedItem;
    });

    const csvOutput = stringify(exportedItems, {
      header: true,
      columns: getColumns(exportedItems),
    });

    const stream = new PassThrough();
    stream.write(csvOutput);
    stream.end();
    const { writeStream, promise } = await this.awsS3Service.putObject(
      `omeka-export/${omekaExport.id}.csv`,
    );
    stream.pipe(writeStream);
    await promise;

    omekaExport.ready = true;
    await this.omekaExportRepository.save(omekaExport);
  }

  async getItemsFromPackages(
    packages: CreateOmekaExportPackageDto[],
  ): Promise<OmekaExportItemDto[]> {
    const package_ids = packages.map((askPackage) => +askPackage.package_id);
    const chunk1 = await this.getChunk1(package_ids);
    const chunk2 = await this.getChunk2(package_ids);
    const chunk3 = await this.getChunk3(package_ids);
    const chunk4 = await this.getChunk4(package_ids);
    const chunk5 = await this.getChunk5(package_ids);
    const chunk6 = await this.getChunk6(package_ids);
    const chunk7 = await this.getChunk7(package_ids);
    const chunk8 = await this.getChunk8(package_ids);
    const chunk9 = await this.getChunk9(package_ids);
    const chunk10 = await this.getChunk10(package_ids);
    const chunk11 = await this.getChunk11(package_ids);
    const chunk12 = await this.getChunk12(package_ids);
    const chunk13 = await this.getChunk13(package_ids);
    const chunk14 = await this.getChunk14(package_ids);
    const chunk15 = await this.getChunk15(package_ids);
    const items = new Map<number, OmekaExportItemDto>();
    for (const item1 of chunk1) {
      const item = items.get(item1.item_id);
      if (!item) {
        const projectId = packages.find(
          (pck) => +pck.package_id === item1.package_id,
        );
        const newItem = new OmekaExportItemDto();
        newItem.item_id = item1.item_id.toString() || '';
        if (projectId) {
          newItem.project_id = projectId.project_id || undefined;
        }
        newItem.package = item1.package || '';
        newItem.package_id = item1.package_id.toString() || '';
        newItem.package_ideascube_name = item1.package_ideascube_name || '';
        newItem.title = item1.title || '';
        items.set(item1.item_id, newItem);
      }
    }
    for (const item2 of chunk2) {
      const item = items.get(item2.item_id);
      if (item) {
        item.description = item2.description || '';
      }
    }

    const langs = new Map<number, string[]>();
    for (const item3 of chunk3) {
      if (item3.lang) {
        const lang = langs.get(item3.item_id);
        if (lang) {
          if (item3.lang.includes(',')) {
            lang.push(...item3.lang.split(','));
          } else {
            lang.push(item3.lang);
          }
        } else {
          if (item3.lang.includes(',')) {
            langs.set(item3.item_id, [...item3.lang.split(',')]);
          } else {
            langs.set(item3.item_id, [item3.lang]);
          }
        }
      }
    }
    for (const [key, value] of langs) {
      const item = items.get(key);
      if (item) {
        let index = 0;
        item.lang = value.join(',');
        for (const lang of value) {
          item[`lang_${index + 1}`] = lang;
          index++;
        }
      }
    }

    for (const item4 of chunk4) {
      const item = items.get(item4.item_id);
      if (item) {
        item.format = item4.format || '';
      }
    }

    const audiences = new Map<number, string[]>();
    for (const item5 of chunk5) {
      if (item5.audience) {
        const audience = audiences.get(item5.item_id);
        if (audience) {
          if (item5.audience.includes(',')) {
            audience.push(...item5.audience.split(','));
          } else {
            audience.push(item5.audience);
          }
        } else {
          if (item5.audience.includes(',')) {
            audiences.set(item5.item_id, [...item5.audience.split(',')]);
          } else {
            audiences.set(item5.item_id, [item5.audience]);
          }
        }
      }
    }
    for (const [key, value] of audiences) {
      const item = items.get(key);
      if (item) {
        let index = 0;
        item.audience = value.join(',');
        for (const audience of value) {
          item[`audience_${index + 1}`] = audience;
          index++;
        }
      }
    }

    for (const item6 of chunk6) {
      const item = items.get(item6.item_id);
      if (item) {
        item.auteur = item6.auteur || '';
      }
    }
    for (const item7 of chunk7) {
      const item = items.get(item7.item_id);
      if (item) {
        item.editeur = item7.editeur || '';
      }
    }
    for (const item8 of chunk8) {
      const item = items.get(item8.item_id);
      if (item) {
        item.licence = item8.licence || '';
      }
    }
    for (const item9 of chunk9) {
      const item = items.get(item9.item_id);
      if (item) {
        item.duration = item9.duration || '';
      }
    }
    for (const item10 of chunk10) {
      const item = items.get(item10.item_id);
      if (item) {
        item.filename = item10.filename || '';
        if (item.filename !== '') {
          item.file_URL = `http://omeka.tm.bsf-intranet.org/files/original/${item.filename}`;
          item.file_URL_S3 = `https://omeka-media-prod-eu-west-1-481031604408.s3.eu-west-1.amazonaws.com/original/${item.filename}`;
          item.thumbnail_URL = `http://omeka.tm.bsf-intranet.org/files/square_thumbnails/${
            item.filename.split('.')[0]
          }.jpg`;
          item.thumbnail_URL_S3 = `https://omeka-media-prod-eu-west-1-481031604408.s3.eu-west-1.amazonaws.com/square_thumbnails/${
            item.filename.split('.')[0]
          }.jpg`;
        }
        item.mime_type = item10.mime_type || '';
        item.file_size = item10.file_size || '';
      }
    }
    const tags = new Map<number, string[]>();
    for (const item11 of chunk11) {
      if (item11.name) {
        const tag = tags.get(item11.item_id);
        if (tag) {
          tag.push(item11.name);
        } else {
          tags.set(item11.item_id, [item11.name]);
        }
      }
    }
    for (const [key, value] of tags) {
      const item = items.get(key);
      if (item) {
        let index = 0;
        item.tags = value.join(',');
        for (const tag of value) {
          item[`tag_${index + 1}`] = tag;
          index++;
        }
      }
    }
    for (const item12 of chunk12) {
      const item = items.get(item12.item_id);
      if (item) {
        item.collection = item12.collection || '';
      }
    }
    for (const item13 of chunk13) {
      const item = items.get(item13.item_id);
      if (item) {
        item.source = item13.source || '';
      }
    }
    const subtitles = new Map<number, string[]>();
    for (const item14 of chunk14) {
      if (item14.subtitles) {
        const subtitle = subtitles.get(item14.item_id);
        if (subtitle) {
          if (item14.subtitles.includes(',')) {
            subtitle.push(...item14.subtitles.split(','));
          } else {
            subtitle.push(item14.subtitles);
          }
        } else {
          if (item14.subtitles.includes(',')) {
            tags.set(item14.item_id, [...item14.subtitles.split(',')]);
          } else {
            tags.set(item14.item_id, [item14.subtitles]);
          }
        }
      }
    }
    for (const [key, value] of subtitles) {
      const item = items.get(key);
      if (item) {
        let index = 0;
        item.subtitles = value.join(',');
        for (const subtitle of value) {
          item[`subtitles_${index + 1}`] = subtitle;
          index++;
        }
      }
    }
    for (const item15 of chunk15) {
      const item = items.get(item15.item_id);
      if (item) {
        item.subject = item15.subject || '';
      }
    }
    items.forEach((item) => {
      const askPackage = packages.find(
        (ask) => ask.package_id === item.package_id,
      );
      if (askPackage) {
        const custom = { ...askPackage };
        delete custom.package_id;
        item.custom = custom;
      }
    });
    return Array.from(items, (entry) => {
      return entry[1];
    });
  }

  async getChunk1(packages: number[]): Promise<Chunk1[]> {
    const sqlPackages = packages.join(',');
    const query = `
    SELECT package.id as package_id, package.name as package,package.ideascube_name as package_ideascube_name, package_item.item_id as item_id, text.text as title 
    FROM omeka_package_manager_packages as package, omeka_package_manager_packages_contents as package_item, 
    omeka_element_texts as text 
    WHERE package_item.package_id = package.id 
    AND text.record_id = package_item.item_id 
    AND text.element_id = 50 
    AND package.id IN (${sqlPackages});`;
    const result = await this.dataSource.query(query);
    return result as Chunk1[];
  }

  async getChunk2(packages: number[]): Promise<Chunk2[]> {
    const sqlPackages = packages.join(',');
    const query = `
    SELECT package_item.item_id as item_id, text.text as description, package.id as package_id
    FROM omeka_package_manager_packages as package, omeka_package_manager_packages_contents as package_item, 
    omeka_element_texts as text 
    WHERE package_item.package_id = package.id 
    AND text.record_id = package_item.item_id 
    AND text.element_id = 41 
    AND package.id IN (${sqlPackages});`;
    const result = await this.dataSource.query(query);
    return result as Chunk2[];
  }

  async getChunk3(packages: number[]): Promise<Chunk3[]> {
    const sqlPackages = packages.join(',');
    const query = `
    SELECT package_item.item_id as item_id, text.text as lang, package.id as package_id
    FROM omeka_package_manager_packages as package, omeka_package_manager_packages_contents as package_item, 
    omeka_element_texts as text 
    WHERE package_item.package_id = package.id 
    AND text.record_id = package_item.item_id 
    AND text.element_id = 44
    AND package.id IN (${sqlPackages});`;
    const result = await this.dataSource.query(query);
    return result;
  }

  async getChunk4(packages: number[]): Promise<Chunk4[]> {
    const sqlPackages = packages.join(',');
    const query = `
    SELECT package_item.item_id as item_id, text.text as format, package.id as package_id
    FROM omeka_package_manager_packages as package, omeka_package_manager_packages_contents as package_item, 
    omeka_element_texts as text 
    WHERE package_item.package_id = package.id 
    AND text.record_id = package_item.item_id 
    AND text.element_id = 42
    AND package.id IN (${sqlPackages});`;
    const result = await this.dataSource.query(query);
    return result;
  }

  async getChunk5(packages: number[]): Promise<Chunk5[]> {
    const sqlPackages = packages.join(',');
    const query = `
    SELECT package_item.item_id as item_id, text.text as audience, package.id as package_id
    FROM omeka_package_manager_packages as package, omeka_package_manager_packages_contents as package_item, 
    omeka_element_texts as text 
    WHERE package_item.package_id = package.id 
    AND text.record_id = package_item.item_id 
    AND text.element_id = 86
    AND package.id IN (${sqlPackages});`;
    const result = await this.dataSource.query(query);
    return result;
  }

  async getChunk6(packages: number[]): Promise<Chunk6[]> {
    const sqlPackages = packages.join(',');
    const query = `
    SELECT package_item.item_id as item_id, text.text as auteur, package.id as package_id
    FROM omeka_package_manager_packages as package, omeka_package_manager_packages_contents as package_item, 
    omeka_element_texts as text 
    WHERE package_item.package_id = package.id 
    AND text.record_id = package_item.item_id 
    AND text.element_id = 39
    AND package.id IN (${sqlPackages});`;
    const result = await this.dataSource.query(query);
    return result;
  }

  async getChunk7(packages: number[]): Promise<Chunk7[]> {
    const sqlPackages = packages.join(',');
    const query = `
    SELECT package_item.item_id as item_id, text.text as editeur, package.id as package_id
    FROM omeka_package_manager_packages as package, omeka_package_manager_packages_contents as package_item, 
    omeka_element_texts as text 
    WHERE package_item.package_id = package.id 
    AND text.record_id = package_item.item_id 
    AND text.element_id = 45
    AND package.id IN (${sqlPackages});`;
    const result = await this.dataSource.query(query);
    return result;
  }

  async getChunk8(packages: number[]): Promise<Chunk8[]> {
    const sqlPackages = packages.join(',');
    const query = `
    SELECT package_item.item_id as item_id, text.text as licence, package.id as package_id
    FROM omeka_package_manager_packages as package, omeka_package_manager_packages_contents as package_item, 
    omeka_element_texts as text 
    WHERE package_item.package_id = package.id 
    AND text.record_id = package_item.item_id 
    AND text.element_id = 64
    AND package.id IN (${sqlPackages});`;
    const result = await this.dataSource.query(query);
    return result;
  }

  async getChunk9(packages: number[]): Promise<Chunk9[]> {
    const sqlPackages = packages.join(',');
    const query = `
    SELECT package_item.item_id as item_id, text.text as duration, package.id as package_id
    FROM omeka_package_manager_packages as package, omeka_package_manager_packages_contents as package_item, 
    omeka_element_texts as text 
    WHERE package_item.package_id = package.id 
    AND text.record_id = package_item.item_id 
    AND text.element_id = 11
    AND package.id IN (${sqlPackages});`;
    const result = await this.dataSource.query(query);
    return result;
  }

  async getChunk10(packages: number[]): Promise<Chunk10[]> {
    const sqlPackages = packages.join(',');
    const query = `
    SELECT package_item.item_id as item_id, files.filename, files.mime_type, files.size as file_size, package.id as package_id
    FROM omeka_package_manager_packages as package, 
    omeka_package_manager_packages_contents as package_item, 
    omeka_files as files 
    WHERE package_item.package_id = package.id 
    AND files.item_id = package_item.item_id 
    AND package.id IN (${sqlPackages});`;
    const result = await this.dataSource.query(query);
    return result;
  }

  async getChunk11(packages: number[]): Promise<Chunk11[]> {
    const sqlPackages = packages.join(',');
    const query = `
    SELECT package_item.item_id as item_id, tag.name, package.id as package_id
    FROM omeka_package_manager_packages as package, 
    omeka_package_manager_packages_contents as package_item, 
    omeka_tags as tag, omeka_records_tags as tag_item 
    WHERE package_item.package_id = package.id 
    AND tag_item.record_id = package_item.item_id 
    AND tag_item.tag_id = tag.id 
    AND package.id IN (${sqlPackages});`;
    const result = await this.dataSource.query(query);
    return result;
  }

  async getChunk12(packages: number[]): Promise<Chunk12[]> {
    const sqlPackages = packages.join(',');
    const query = `
    SELECT package.id as package_id, item.id as item_id, text as collection
    FROM omeka_package_manager_packages as package
    INNER JOIN omeka_package_manager_packages_contents AS package_item ON package.id = package_item.package_id
    INNER JOIN omeka_items AS item ON package_item.item_id = item.id
    INNER JOIN omeka_collections AS collection ON item.collection_id = collection.id
    INNER JOIN omeka_element_texts AS text ON collection.id = text.record_id
    WHERE package.id IN (${sqlPackages});`;
    const result = await this.dataSource.query(query);
    return result;
  }

  async getChunk13(packages: number[]): Promise<Chunk13[]> {
    const sqlPackages = packages.join(',');
    const query = `
    SELECT package_item.item_id as item_id, text.text as source, package.id as package_id
    FROM omeka_package_manager_packages as package, omeka_package_manager_packages_contents as package_item, 
    omeka_element_texts as text 
    WHERE package_item.package_id = package.id 
    AND text.record_id = package_item.item_id 
    AND text.element_id = 48
    AND package.id IN (${sqlPackages});`;
    const result = await this.dataSource.query(query);
    return result;
  }

  async getChunk14(packages: number[]): Promise<Chunk14[]> {
    const sqlPackages = packages.join(',');
    const query = `
    SELECT package_item.item_id as item_id, text.text as subtitles, package.id as package_id
    FROM omeka_package_manager_packages as package, omeka_package_manager_packages_contents as package_item, 
    omeka_element_texts as text 
    WHERE package_item.package_id = package.id 
    AND text.record_id = package_item.item_id 
    AND text.element_id = 253
    AND package.id IN (${sqlPackages});`;
    const result = await this.dataSource.query(query);
    return result;
  }

  async getChunk15(packages: number[]): Promise<Chunk15[]> {
    const sqlPackages = packages.join(',');
    const query = `
    SELECT package_item.item_id as item_id, text.text as subject, package.id as package_id
    FROM omeka_package_manager_packages as package, omeka_package_manager_packages_contents as package_item, 
    omeka_element_texts as text 
    WHERE package_item.package_id = package.id 
    AND text.record_id = package_item.item_id 
    AND text.element_id = 49
    AND package.id IN (${sqlPackages});`;
    const result = await this.dataSource.query(query);
    return result;
  }
}
