import { Module } from '@nestjs/common';
import { OmekaExportController } from './omeka-export.controller';
import { OmekaExportService } from './omeka-export.service';
import { AwsS3Module } from 'src/aws-s3/aws-s3.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OmekaExport } from './entities/omeka-export.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([OmekaExport], 'OmekaExport'),
    AwsS3Module,
  ],
  controllers: [OmekaExportController],
  providers: [OmekaExportService],
})
export class OmekaExportModule {}
