import { OmekaExportItemDto } from '../dto/omeka-export-item.dto';

export const getColumns = (items: OmekaExportItemDto[]) => {
  let projectIdExists = false;
  let numberOfTagsMax = 0;
  let numberOfLangsMax = 0;
  let numberOfAudiencesMax = 0;
  let numberOfSubtitlesMax = 0;
  const customNames = [];
  for (const item of items) {
    if (item.project_id !== undefined) {
      projectIdExists = true;
    }
    const properties = Object.keys(item);
    for (const property of properties) {
      if (property.startsWith('tag_')) {
        const removeTagFromProperty = property.replace('tag_', '');
        if (+removeTagFromProperty > numberOfTagsMax) {
          numberOfTagsMax = +removeTagFromProperty;
        }
      }
      if (property.startsWith('lang_')) {
        const removeFromProperty = property.replace('lang_', '');
        if (+removeFromProperty > numberOfLangsMax) {
          numberOfLangsMax = +removeFromProperty;
        }
      }
      if (property.startsWith('audience_')) {
        const removeFromProperty = property.replace('audience_', '');
        if (+removeFromProperty > numberOfAudiencesMax) {
          numberOfAudiencesMax = +removeFromProperty;
        }
      }
      if (property.startsWith('subtitles_')) {
        const removeFromProperty = property.replace('ausubtitles_dience_', '');
        if (+removeFromProperty > numberOfSubtitlesMax) {
          numberOfSubtitlesMax = +removeFromProperty;
        }
      }
      if (property.startsWith('custom_')) {
        if (!customNames.includes(property)) {
          customNames.push(property);
        }
      }
    }
  }
  const optionalColumns: any = [];
  if (projectIdExists) {
    optionalColumns.push({ key: 'project_id', header: 'project_id' });
  }
  const langs: any = [];
  for (let i = 0; i < numberOfLangsMax; i++) {
    langs.push({ key: `lang_${i + 1}`, header: `lang_${i + 1}` });
  }
  const audiences: any = [];
  for (let i = 0; i < numberOfAudiencesMax; i++) {
    audiences.push({ key: `audience_${i + 1}`, header: `audience_${i + 1}` });
  }
  const tags: any = [];
  for (let i = 0; i < numberOfTagsMax; i++) {
    tags.push({ key: `tag_${i + 1}`, header: `tag_${i + 1}` });
  }
  const subtitles: any = [];
  for (let i = 0; i < numberOfSubtitlesMax; i++) {
    subtitles.push({ key: `subtitles_${i + 1}`, header: `subtitles_${i + 1}` });
  }
  const customs: any = [];
  for (let i = 0; i < customNames.length; i++) {
    customs.push({ key: customNames[i], header: customNames[i] });
  }
  return [
    ...optionalColumns,
    { key: 'package_id', header: 'package_id' },
    { key: 'item_id', header: 'item_id' },
    { key: 'package', header: 'package' },
    { key: 'package_ideascube_name', header: 'package_ideascube_name' },
    { key: 'title', header: 'title' },
    { key: 'description', header: 'description' },
    { key: 'lang', header: 'lang' },
    ...langs,
    { key: 'subtitles', header: 'subtitles' },
    ...subtitles,
    { key: 'format', header: 'format' },
    { key: 'audience', header: 'audience' },
    ...audiences,
    { key: 'source', header: 'source' },
    { key: 'auteur', header: 'auteur' },
    { key: 'editeur', header: 'editeur' },
    { key: 'licence', header: 'licence' },
    { key: 'duration', header: 'duration' },
    { key: 'mime_type', header: 'mime_type' },
    { key: 'file_size', header: 'file_size' },
    { key: 'filename', header: 'filename' },
    { key: 'file_URL', header: 'file_URL' },
    { key: 'thumbnail_URL', header: 'thumbnail_URL' },
    { key: 'file_URL_S3', header: 'file_URL_S3' },
    { key: 'thumbnail_URL_S3', header: 'thumbnail_URL_S3' },
    { key: 'collection', header: 'collection' },
    { key: 'tags', header: 'tags' },
    ...tags,
    ...customs,
  ];
};
