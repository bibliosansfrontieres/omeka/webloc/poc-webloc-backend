import { CreateOmekaExportDto } from '../dto/create-omeka-export.dto';
import { OmekaExport } from '../entities/omeka-export.entity';

export interface AskedOmekaExport {
  omekaExport: OmekaExport;
  createOmekaExportDto: CreateOmekaExportDto;
}
