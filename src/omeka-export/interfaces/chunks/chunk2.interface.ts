export interface Chunk2 {
  package_id: number;
  item_id: number;
  description: string;
}
