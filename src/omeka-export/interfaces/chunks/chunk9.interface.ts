export interface Chunk9 {
  item_id: number;
  package_id: number;
  duration: string;
}
