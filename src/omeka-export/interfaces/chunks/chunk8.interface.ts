export interface Chunk8 {
  package_id: number;
  item_id: number;
  licence: string;
}
