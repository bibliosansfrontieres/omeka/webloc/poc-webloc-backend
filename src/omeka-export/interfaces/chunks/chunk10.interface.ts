export interface Chunk10 {
  item_id: number;
  package_id: number;
  filename: string;
  mime_type: string;
  file_size: string;
}
