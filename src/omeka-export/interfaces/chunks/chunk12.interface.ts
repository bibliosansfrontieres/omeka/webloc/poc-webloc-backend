export interface Chunk12 {
  package_id: number;
  item_id: number;
  collection: string;
}
