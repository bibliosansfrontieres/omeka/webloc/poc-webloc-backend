export interface Chunk1 {
  package_id: number;
  package: string;
  package_ideascube_name: string;
  item_id: number;
  title: string;
}
