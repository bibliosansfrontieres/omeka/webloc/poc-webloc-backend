import {
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Post,
  Res,
} from '@nestjs/common';
import { CreateOmekaExportDto } from './dto/create-omeka-export.dto';
import { OmekaExportService } from './omeka-export.service';
import { Response } from 'express';
import { AwsS3Service } from 'src/aws-s3/aws-s3.service';
import { Readable } from 'stream';
import * as moment from 'moment';

@Controller('omeka-export')
export class OmekaExportController {
  constructor(
    private awsS3Service: AwsS3Service,
    private omekaExportService: OmekaExportService,
  ) {}

  @Post()
  askExport(@Body() createOmekaExportDto: CreateOmekaExportDto) {
    return this.omekaExportService.askExport(createOmekaExportDto);
  }

  @Post('all')
  askExportAllPackages(@Body('email') email: string) {
    return this.omekaExportService.askExportAllPackages(email);
  }

  @Get('download/:id')
  async downloadExport(@Param('id') id: string, @Res() res: Response) {
    const omekaExport = await this.omekaExportService.getExport(+id);
    if (omekaExport === null) throw new NotFoundException();
    const date = moment(omekaExport.timestamp).format('YYYY-MM-DD');

    res.set({
      'Content-Disposition': `attachment; filename="${date}-${omekaExport.packages}packages.csv"`,
    });

    const object = await this.awsS3Service.getObject(`omeka-export/${id}.csv`);
    if (!object.Body) throw new NotFoundException();

    const stream = object.Body as Readable;
    stream.pipe(res);
  }

  @Get(':page')
  getExports(@Param('page') page: string) {
    return this.omekaExportService.getExports(+page);
  }
}
