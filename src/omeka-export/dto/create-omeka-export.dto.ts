import { CreateOmekaExportPackageDto } from './create-omeka-export-package.dto';
import { CreateOmekaExportProjectDto } from './create-omeka-export-project.dto';

export class CreateOmekaExportDto {
  email: string;
  packages: CreateOmekaExportPackageDto[];
  projects: CreateOmekaExportProjectDto[];
}
