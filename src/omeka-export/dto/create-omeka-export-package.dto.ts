export class CreateOmekaExportPackageDto {
  package_id?: string;
  [key: string]: string;
}
