export class CreateOmekaExportProjectDto {
  project_id?: string;
  [key: string]: string;
}
