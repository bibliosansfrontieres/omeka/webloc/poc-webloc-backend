export class OmekaExportItemDto {
  project_id?: string;
  package_id: string;
  item_id: string;
  package: string;
  package_ideascube_name: string;
  title: string;
  description: string;
  lang: string;
  format: string;
  audience: string;
  source: string;
  auteur: string;
  editeur: string;
  licence: string;
  duration: string;
  mime_type: string;
  file_size: string;
  filename: string;
  file_URL: string;
  thumbnail_URL: string;
  file_URL_S3: string;
  thumbnail_URL_S3: string;
  collection: string;
  tags: string;
  custom: object;
  [key: string]: any;

  constructor() {
    this.project_id = undefined;
    this.package_id = '';
    this.item_id = '';
    this.package = '';
    this.package_ideascube_name = '';
    this.title = '';
    this.description = '';
    this.lang = '';
    this.format = '';
    this.audience = '';
    this.source = '';
    this.auteur = '';
    this.editeur = '';
    this.licence = '';
    this.duration = '';
    this.mime_type = '';
    this.file_size = '';
    this.filename = '';
    this.file_URL = '';
    this.thumbnail_URL = '';
    this.file_URL_S3 = '';
    this.thumbnail_URL_S3 = '';
    this.collection = '';
    this.tags = '';
    this.custom = {};
  }
}
