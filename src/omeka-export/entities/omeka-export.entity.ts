import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { CreateOmekaExportDto } from '../dto/create-omeka-export.dto';

@Entity()
export class OmekaExport {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column()
  timestamp: number;

  @Column()
  packages: number;

  @Column()
  ready: boolean;

  constructor(createOmekaExportDto: CreateOmekaExportDto) {
    if (!createOmekaExportDto) return;
    this.ready = false;
    this.packages = createOmekaExportDto.packages.length;
    this.email = createOmekaExportDto.email;
    this.timestamp = Date.now();
  }
}
