import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassThrough } from 'stream';
import { Upload } from '@aws-sdk/lib-storage';
import {
  PutObjectCommand,
  DeleteObjectCommand,
  S3Client,
  GetObjectCommand,
} from '@aws-sdk/client-s3';
import * as ping from 'ping';
import fetch from 'node-fetch';
import * as moment from 'moment';

@Injectable()
export class AwsS3Service {
  private awsHost = 'http://169.254.169.254';
  private s3: S3Client;
  private region: string;
  private role: string | undefined;
  private token: string | undefined;
  private accessKeyId: string | undefined;
  private secretAccessKey: string | undefined;
  private expiration: string;
  private bucket: string;
  public url: string;

  constructor(private configService: ConfigService) {
    this.region = this.configService.get('S3_REGION');
    this.bucket = this.configService.get('S3_BUCKET_NAME');
    this.url = `https://${this.bucket}.s3.${this.region}.amazonaws.com`;
  }

  async onModuleInit() {
    let pingResult = false;
    try {
      const result = await fetch(this.awsHost);
      if (result.ok) {
        pingResult = true;
      }
    } catch {}
    if (!pingResult) {
      this.accessKeyId = this.configService.get('S3_ACCESS_KEY');
      this.secretAccessKey = this.configService.get('S3_ACCESS_SECRET');
      if (!this.accessKeyId || !this.secretAccessKey) {
        Logger.error(
          'AWS host not found. If you are in dev environment, please provide S3_ACCESS_KEY and S3_ACCESS_SECRET env variables.',
          'AwsS3Service',
        );
        process.exit(-1);
      } else {
        Logger.warn(
          'AWS host not found, using env variables instead',
          'AwsS3Service',
        );
        this.s3 = new S3Client({
          region: this.region,
          credentials: {
            accessKeyId: this.accessKeyId,
            secretAccessKey: this.secretAccessKey,
          },
        });
      }
    } else {
      await this.getCredentials();
    }
  }

  async needNewCredentials() {
    if (
      this.token !== undefined &&
      parseInt(this.expiration) <= parseInt((Date.now() / 1000).toString())
    ) {
      await this.getCredentials();
    }
  }

  async getRoleName(): Promise<string> {
    try {
      const getIamInfo = await fetch(
        `${this.awsHost}/latest/meta-data/iam/info`,
      );
      const iamInfo = await getIamInfo.json();
      const profileSplit = iamInfo.InstanceProfileArn.split('/');
      return profileSplit[profileSplit.length - 1];
    } catch {
      Logger.error('Could not get profile from IAM info', 'AwsS3Service');
      return '';
    }
  }

  async getCredentials() {
    const roleName = await this.getRoleName();
    try {
      const getCredentials = await fetch(
        `${this.awsHost}/latest/meta-data/iam/security-credentials/${roleName}`,
      );
      const credentials = await getCredentials.json();
      this.accessKeyId = credentials.AccessKeyId;
      this.secretAccessKey = credentials.SecretAccessKey;
      this.expiration = moment(credentials.Expiration).utc().format('X');
      this.token = credentials.Token;
      this.s3 = new S3Client({
        region: this.region,
        credentials: {
          accessKeyId: this.accessKeyId,
          secretAccessKey: this.secretAccessKey,
          sessionToken: this.token,
        },
      });
    } catch {
      Logger.error('Could not get credentials', 'AwsS3Service');
      process.exit(-1);
    }
  }

  async putObject(key: string) {
    await this.needNewCredentials();
    const pass = new PassThrough();
    const parallelUploads3 = new Upload({
      client: this.s3,
      params: {
        Bucket: this.bucket,
        Key: key,
        Body: pass,
      },
      leavePartsOnError: true,
    });
    return {
      writeStream: pass,
      promise: parallelUploads3.done(),
    };
  }

  async deleteObject(key: string) {
    await this.needNewCredentials();
    const command = new DeleteObjectCommand({ Bucket: this.bucket, Key: key });
    await this.s3.send(command);
  }

  async getObject(key: string) {
    await this.needNewCredentials();
    const command = new GetObjectCommand({ Bucket: this.bucket, Key: key });
    return this.s3.send(command);
  }

  getObjectUrl(key: string) {
    return `${this.url}/${key}`;
  }
}
