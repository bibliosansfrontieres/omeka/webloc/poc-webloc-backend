import { Controller, Get, Header, Res } from '@nestjs/common';
import { LoggerService } from './logger.service';
import { Response } from 'express';

@Controller('logger')
export class LoggerController {
  constructor(private loggerService: LoggerService) {}
  @Get()
  @Header('Content-Type', 'text/plain; charset=utf-8')
  getLogs(@Res() res: Response) {
    const file = this.loggerService.getLogs();
    file.pipe(res);
  }
}
