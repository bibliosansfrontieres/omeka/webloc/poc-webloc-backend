import { Injectable } from '@nestjs/common';
import { join } from 'path';
import * as fs from 'fs';

@Injectable()
export class LoggerService {
  private logsFilePath: string;

  constructor() {
    this.logsFilePath = join(__dirname, '../../data/logs.txt');
  }

  getLogs() {
    return fs.createReadStream(this.logsFilePath, { encoding: 'utf8' });
  }
}
