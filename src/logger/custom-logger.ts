import { ConsoleLogger } from '@nestjs/common';
import * as moment from 'moment';
import { join } from 'path';
import * as fs from 'fs';

export class CutomLogger extends ConsoleLogger {
  private logsFilePath: string;

  constructor() {
    super();
    this.logsFilePath = join(__dirname, '../../data/logs.txt');
    this.checkLogsFileExists();
  }

  private checkLogsFileExists() {
    const isFileExists = fs.existsSync(this.logsFilePath);
    if (!isFileExists) {
      fs.writeFileSync(this.logsFilePath, '');
    }
  }

  customLog(type: string, message: any, context?: string) {
    const date = moment(Date.now()).format('DD/MM/YYYY HH:mm:ss');
    const log = `${date} - ${type} [${context}] ${message}\r\n`;
    fs.appendFileSync(this.logsFilePath, log, { encoding: 'utf8' });
  }

  log(message: any, context?: string) {
    super.log(message, context);
    this.customLog('LOG', message, context);
  }

  error(message: any, context?: string) {
    super.error(message, context);
    this.customLog('ERROR', message, context);
  }
}
