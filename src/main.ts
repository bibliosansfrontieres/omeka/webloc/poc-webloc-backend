import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { CutomLogger } from './logger/custom-logger';
import * as fs from 'fs';
import { join } from 'path';
import { NestExpressApplication } from '@nestjs/platform-express';

function checkDataFolder() {
  const dataFolderPath = join(__dirname, '../data');
  const isDataFolderExists = fs.existsSync(dataFolderPath);
  if (!isDataFolderExists) {
    fs.mkdirSync(dataFolderPath);
  }
}

async function bootstrap() {
  checkDataFolder();
  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    logger: new CutomLogger(),
  });
  const config = app.get(ConfigService);
  app.enableCors();
  app.useBodyParser('json', { limit: '50mb' });
  await app.listen(config.get('PORT'));
}
bootstrap();
