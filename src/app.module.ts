import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigSchema } from './config/configuration.schema';
import { SharepointModule } from './sharepoint/sharepoint.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WatcherModule } from './watcher/watcher.module';
import { ScheduleModule } from '@nestjs/schedule';
import { SaveModule } from './save/save.module';
import { Save } from './save/entities/save.entity';
import { FileMetadata } from './save/entities/file-metadata.entity';
import { LocalFileModule } from './local-file/local-file.module';
import { LocalFolderModule } from './local-folder/local-folder.module';
import { LocalFile } from './local-file/entities/local-file.entity';
import { LocalFolder } from './local-folder/entities/local-folder.entity';
import { BdExtModule } from './bd-ext/bd-ext.module';
import { Package } from './save/entities/package.entity';
import { SocketModule } from './socket/socket.module';
import { LoggerModule } from './logger/logger.module';
import { AwsS3Module } from './aws-s3/aws-s3.module';
import { DataSource } from 'typeorm';
import { OmekaExportModule } from './omeka-export/omeka-export.module';
import { OmekaExport } from './omeka-export/entities/omeka-export.entity';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    ConfigModule.forRoot({
      isGlobal: true,
      validationSchema: ConfigSchema,
    }),
    TypeOrmModule.forRootAsync({
      name: 'WebLoc',
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: 'sqlite',
        database: `data/${configService.get<string>('SQLITE_NAME')}`,
        entities: [LocalFile, LocalFolder, Save, FileMetadata, Package],
        synchronize: true,
      }),
      inject: [ConfigService],
    }),
    TypeOrmModule.forRoot({
      type: 'sqlite',
      name: 'OmekaExport',
      database: 'data/omeka-export.db',
      entities: [OmekaExport],
      synchronize: true,
    }),
    TypeOrmModule.forRootAsync({
      name: 'Omeka',
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: 'mysql',
        url: configService.get('OMEKA_MYSQL_URL'),
      }),
      dataSourceFactory: async (options) => {
        const dataSource = await new DataSource(options).initialize();
        return dataSource;
      },
      inject: [ConfigService],
    }),
    SharepointModule,
    WatcherModule,
    SaveModule,
    LocalFileModule,
    LocalFolderModule,
    BdExtModule,
    SocketModule,
    LoggerModule,
    AwsS3Module,
    OmekaExportModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
