import { Module } from '@nestjs/common';
import { SharepointService } from './sharepoint.service';
import { AwsS3Module } from 'src/aws-s3/aws-s3.module';

@Module({
  imports: [AwsS3Module],
  providers: [SharepointService],
  exports: [SharepointService],
})
export class SharepointModule {}
