import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { SharepointApi, SharepointApiOptions } from 'sharepoint-api-wrapper';
import { SpFile } from 'sharepoint-api-wrapper/dist/interfaces/microsoft/sp-file.interface';
import { SpFolder } from 'sharepoint-api-wrapper/dist/interfaces/microsoft/sp-folder.interface';
import { LocalFolder } from 'src/local-folder/entities/local-folder.entity';
import * as fs from 'fs';
import * as moment from 'moment';
import { AwsS3Service } from 'src/aws-s3/aws-s3.service';
import { LocalFile } from 'src/local-file/entities/local-file.entity';

/**
 * SharepointService
 * @property {SharepointApi} sharepointApi - SharepointApi object to use Sharepoint api
 * @property {string} sharepointUrl - Global URL of Sharepoint
 * @property {string} folderOfImports - Name of folder where to import folders/files
 * @property {string} folderOfArchives - Name of folder where to archive folders/files that has been imported in Omeka
 */
@Injectable()
export class SharepointService {
  public sharepointApi: SharepointApi;
  private sharepointUrl: string;
  private folderOfImports: string;
  private folderOfArchives: string;

  constructor(
    private awsS3Service: AwsS3Service,
    configService: ConfigService,
  ) {
    this.folderOfImports = configService.get<string>('FOLDER_OF_IMPORTS');
    this.folderOfArchives = configService.get<string>('FOLDER_OF_ARCHIVES');
    this.sharepointUrl = configService.get<string>('SHAREPOINT_URL');
    const options: SharepointApiOptions = {
      authOptions: {
        clientId: configService.get<string>('SHAREPOINT_CLIENT_ID'),
        clientSecret: configService.get<string>('SHAREPOINT_CLIENT_SECRET'),
        realm: configService.get<string>('SHAREPOINT_TENANT_ID'),
      },
      url: this.sharepointUrl,
      siteName: configService.get<string>('SHAREPOINT_SITE_NAME'),
      baseFolder: configService.get<string>('SHAREPOINT_BASE_FOLDER'),
    };
    try {
      this.sharepointApi = new SharepointApi(options);
    } catch (e) {
      console.error('ERROR_DEBUG', e);
      throw new Error();
    }
  }

  /* istanbul ignore next */
  async onModuleInit() {
    await this.checkExistingFolderOrCrash(this.folderOfImports);
    await this.checkExistingFolderOrCrash(this.folderOfArchives);
  }

  /**
   * Check if a folder exists locally or crash
   * @param {string} folderName - Name of folder to check
   * @returns {Promise<void>}
   */
  /* istanbul ignore next */
  private async checkExistingFolderOrCrash(folderName: string): Promise<void> {
    try {
      const folder = await this.sharepointApi.getFolder(folderName);
      if (!folder) {
        throw new Error(
          `You need to create a folder "${folderName}" in Sharepoint`,
        );
      } else {
        Logger.log(
          `Folder "${folderName}" found in Sharepoint site.`,
          'SharepointService',
        );
      }
    } catch (e) {
      console.error('ERROR_DEBUG', e);
    }
  }

  /**
   * Get SpFolders that are in folder of imports in Sharepoint
   * @returns {Promise<SpFolder[] | undefined>}
   */
  /* istanbul ignore next */
  async getFolders(): Promise<SpFolder[] | undefined> {
    try {
      const folders = await this.sharepointApi.getFolders(this.folderOfImports);
      return folders;
    } catch (e) {
      console.error('ERROR_DEBUG', e);
      return undefined;
    }
  }

  /**
   * Get SpFiles from a Sharepoint folder
   * @param {string} folderRelativeUrl - SpFolder serverRelativeUrl where to get files
   * @returns {Promise<SpFile[] | undefined>}
   */
  /* istanbul ignore next */
  async getFiles(folderRelativeUrl: string): Promise<SpFile[] | undefined> {
    try {
      const files = await this.sharepointApi.getFiles(folderRelativeUrl);
      return files;
    } catch (e) {
      console.error('ERROR_DEBUG', e);
      return undefined;
    }
  }

  /**
   * Download a file from Sharepoint
   * @param {string} fileRelativeUrl - SpFile serverRelativeUrl to download
   * @returns {Promise<boolean>} - Returns undefined if any error occured
   */
  /* istanbul ignore next */
  async download(
    localFile: LocalFile,
    downloadStream: fs.WriteStream,
  ): Promise<boolean> {
    try {
      await this.sharepointApi.downloadFileById(
        localFile.uniqueId,
        downloadStream,
      );
      const readable = fs.createReadStream(downloadStream.path);
      const { writeStream, promise } = await this.awsS3Service.putObject(
        localFile.awsKey,
      );
      readable.pipe(writeStream);
      Logger.log(`Uploading "${localFile.name}" to S3`, 'SharepointService');
      try {
        const result = await promise;
        if (
          result &&
          result.$metadata &&
          result.$metadata.httpStatusCode === 200
        ) {
          return true;
        }
      } catch (e) {
        return false;
      }
      return false;
    } catch (e) {
      console.error('ERROR_DEBUG', e);
      return false;
    }
  }

  async moveFolder(localFolder: LocalFolder) {
    const date = moment(Date.now()).format('DD-MM-YYYY');
    const dateFolder = await this.sharepointApi.getFolder(
      `${this.folderOfArchives}/${date}`,
    );
    if (!dateFolder) {
      await this.sharepointApi.createFolder(
        `${date}`,
        this.folderOfArchives + '/',
      );
    }
    const folderUrlSplit = localFolder.serverRelativeUrl.split('/');
    const folderName = folderUrlSplit[folderUrlSplit.length - 1];
    await this.sharepointApi.moveFolder(
      localFolder.serverRelativeUrl,
      `${this.folderOfArchives}/${date}`,
      `${folderName}-${Date.now()}`,
    );
  }
}
