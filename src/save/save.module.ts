import { forwardRef, Module } from '@nestjs/common';
import { SaveService } from './save.service';
import { SaveController } from './save.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Save } from './entities/save.entity';
import { FileMetadata } from './entities/file-metadata.entity';
import { LocalFileModule } from '../local-file/local-file.module';
import { LocalFolderModule } from '../local-folder/local-folder.module';
import { Package } from './entities/package.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Save, FileMetadata, Package], 'WebLoc'),
    forwardRef(() => LocalFileModule),
    forwardRef(() => LocalFolderModule),
  ],
  controllers: [SaveController],
  providers: [SaveService],
  exports: [SaveService],
})
export class SaveModule {}
