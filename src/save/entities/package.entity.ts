import { LocalFolder } from '../../local-folder/entities/local-folder.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CreateSaveDto } from '../dto/create-save.dto';
import { FileMetadata } from './file-metadata.entity';
import { Save } from './save.entity';
import { CreatePackageDto } from '../dto/create-package.dto';

@Entity()
export class Package {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  uniqueId: string;

  @Column()
  index: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column()
  goal: string;

  @Column()
  collection: string;

  @Column()
  audience: string;

  @Column()
  language: string;

  @ManyToOne(() => Save, (save) => save.packages, { onDelete: 'CASCADE' })
  save: Save;

  constructor(createPackageDto: CreatePackageDto, save: Save) {
    if (!createPackageDto) return;
    this.save = save;
    this.index = createPackageDto.index;
    this.uniqueId = createPackageDto.id;
    this.name = createPackageDto.name;
    this.description = createPackageDto.description;
    this.goal = createPackageDto.goal;
    this.collection = createPackageDto.collection;
    this.audience = createPackageDto.audience;
    this.language = createPackageDto.language;
  }
}
