import { LocalFolder } from '../../local-folder/entities/local-folder.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CreateSaveDto } from '../dto/create-save.dto';
import { FileMetadata } from './file-metadata.entity';
import { Package } from './package.entity';

@Entity()
export class Save {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  iso: string;

  @Column()
  timestamp: string;

  @ManyToOne(() => LocalFolder, (localFolder) => localFolder.saves, {
    onDelete: 'CASCADE',
  })
  localFolder: LocalFolder;

  @OneToMany(() => FileMetadata, (fileMetadata) => fileMetadata.save, {
    onDelete: 'CASCADE',
  })
  fileMetadatas: FileMetadata[];

  @OneToMany(() => Package, (packageEntity) => packageEntity.save)
  packages: Package[];

  @Column({ nullable: true })
  email: string | null;

  @Column()
  isExported: boolean;

  constructor(
    createSaveDto: CreateSaveDto,
    localFolder: LocalFolder,
    isExported?: boolean,
  ) {
    if (!createSaveDto) return;
    this.name = createSaveDto.name;
    this.iso = createSaveDto.iso;
    this.timestamp = Date.now().toString();
    this.localFolder = localFolder;
    this.email = createSaveDto.email ? createSaveDto.email : null;
    this.isExported = isExported ? isExported : false;
  }
}
