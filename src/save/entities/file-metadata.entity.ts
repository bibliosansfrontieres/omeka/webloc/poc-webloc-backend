import { LocalFile } from '../../local-file/entities/local-file.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { CreateFileMetadataDto } from '../dto/create-file-metadata.dto';
import { Save } from './save.entity';

@Entity()
export class FileMetadata {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => LocalFile, (localFile) => localFile.fileMetadatas, {
    onDelete: 'CASCADE',
  })
  localFile: LocalFile;

  @ManyToOne(() => Save, (save) => save.fileMetadatas, { onDelete: 'CASCADE' })
  save: Save;

  /* Metadatas */

  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  creator: string;

  @Column()
  publisher: string;

  @Column()
  dateCreated: string;

  @Column()
  language: string;

  @Column()
  license: string;

  @Column()
  source: string;

  @Column()
  audience: string;

  @Column()
  collection: string;

  @Column()
  tag11: string;

  @Column()
  tag12: string;

  @Column()
  tag13: string;

  @Column()
  collection2: string;

  @Column()
  tag21: string;

  @Column()
  tag22: string;

  @Column()
  tag23: string;

  @Column()
  geography1: string;

  @Column()
  geography2: string;

  @Column()
  temporality1: string;

  @Column()
  temporality2: string;

  @Column()
  duration: string;

  @Column()
  subtitles: string;

  @Column()
  size: string;

  @Column()
  ignore: boolean;

  @Column()
  package: string;

  constructor(
    createFileMetadataDto: CreateFileMetadataDto,
    localFile: LocalFile,
    save: Save,
  ) {
    if (!createFileMetadataDto) return;
    this.localFile = localFile;
    this.save = save;

    /* Metadatas */
    this.title = createFileMetadataDto.title;
    this.description = createFileMetadataDto.description;
    this.collection = createFileMetadataDto.collection;
    this.collection2 = createFileMetadataDto.collection2;
    this.creator = createFileMetadataDto.creator;
    this.publisher = createFileMetadataDto.publisher;
    this.dateCreated = createFileMetadataDto.dateCreated;
    this.language = createFileMetadataDto.language;
    this.license = createFileMetadataDto.license;
    this.source = createFileMetadataDto.source;
    this.audience = createFileMetadataDto.audience;
    this.subtitles = createFileMetadataDto.subtitles;
    this.duration = createFileMetadataDto.duration;
    this.size = createFileMetadataDto.size;
    this.ignore = createFileMetadataDto.ignore;
    this.tag11 = createFileMetadataDto.tag11;
    this.tag12 = createFileMetadataDto.tag12;
    this.tag13 = createFileMetadataDto.tag13;
    this.tag21 = createFileMetadataDto.tag21;
    this.tag22 = createFileMetadataDto.tag22;
    this.tag23 = createFileMetadataDto.tag23;
    this.geography1 = createFileMetadataDto.geography1;
    this.geography2 = createFileMetadataDto.geography2;
    this.temporality1 = createFileMetadataDto.temporality1;
    this.temporality2 = createFileMetadataDto.temporality2;
    this.package = createFileMetadataDto.package;
  }
}
