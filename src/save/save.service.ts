import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { LocalFileService } from '../local-file/local-file.service';
import { LocalFolderService } from '../local-folder/local-folder.service';
import { Repository } from 'typeorm';
import { CreateSaveDto } from './dto/create-save.dto';
import { FileMetadata } from './entities/file-metadata.entity';
import { Save } from './entities/save.entity';
import { sortSaves } from 'src/utils/sort-saves';
import { LocalFolder } from 'src/local-folder/entities/local-folder.entity';
import { Package } from './entities/package.entity';

/**
 * SaveService
 */
@Injectable()
export class SaveService {
  constructor(
    @InjectRepository(FileMetadata, 'WebLoc')
    private fileMetadataRepository: Repository<FileMetadata>,
    @InjectRepository(Save, 'WebLoc')
    private saveRepository: Repository<Save>,
    @InjectRepository(Package, 'WebLoc')
    private packageRepository: Repository<Package>,
    @Inject(forwardRef(() => LocalFolderService))
    private localFolderService: LocalFolderService,
    private localFileService: LocalFileService,
  ) {}

  /**
   * Create a save
   * @param {CreateSaveDto} createSaveDto - Informations needed to create a save and files metadata
   * @returns {Promise<Save | undefined>} - Returns undefined if LocalFolder not found
   */
  /* istanbul ignore next */
  async create(
    createSaveDto: CreateSaveDto,
    localFolder?: LocalFolder,
    isExported?: boolean,
  ): Promise<Save | undefined> {
    if (!localFolder) {
      localFolder = await this.localFolderService.findById(
        createSaveDto.folderId,
      );
    }
    if (!localFolder) return undefined;
    let save = new Save(createSaveDto, localFolder, isExported);
    if (save.email) {
      const saves = await this.saveRepository.find({
        where: { email: save.email },
      });
      await this.saveRepository.remove(saves);
    }
    save = await this.saveRepository.save(save);
    await this.createFileMetadatas(createSaveDto, save);
    await this.createPackages(createSaveDto, save);
    return save;
  }

  /**
   * Create a FileMetadata
   * @param {CreateSaveDto} createSaveDto - Informations needed to create FileMetadata
   * @param {Save} save - Save linked to FileMetadata
   * @returns {Promise<void>}
   */
  /* istanbul ignore next */
  async createFileMetadatas(
    createSaveDto: CreateSaveDto,
    save: Save,
  ): Promise<void> {
    const fileMetadatas = [];
    for (const fileMetadataDto of createSaveDto.fileMetadatas) {
      const localFile = await this.localFileService.findById(
        fileMetadataDto.localFile.id,
      );
      const fileMetadata = new FileMetadata(fileMetadataDto, localFile, save);
      fileMetadatas.push(fileMetadata);
    }
    this.fileMetadataRepository.save(fileMetadatas);
  }

  /**
   * Create a Package
   * @param {CreateSaveDto} createSaveDto - Informations needed to create Packages
   * @param {Save} save - Save linked to FileMetadata
   * @returns {Promise<void>}
   */
  /* istanbul ignore next */
  async createPackages(
    createSaveDto: CreateSaveDto,
    save: Save,
  ): Promise<void> {
    const packages = [];
    for (const packageDto of createSaveDto.packages) {
      const packageEntity = new Package(packageDto, save);
      packages.push(packageEntity);
    }
    this.packageRepository.save(packages);
  }

  /**
   * Find FileMetadatas with Save ID
   * @param {number} id - ID of the Save
   * @returns {Promise<FileMetadata[] | undefined>} - Returns undefined if Save not found
   */
  /* istanbul ignore next */
  async findSaveById(id: number): Promise<Save | undefined> {
    const save = await this.saveRepository.findOne({
      where: { id },
      relations: ['fileMetadatas', 'fileMetadatas.localFile', 'packages'],
    });
    if (!save) return undefined;
    return save;
  }

  /**
   * Find all Saves for a LocalFolder id
   * @param {number} folderId - LocalFolder's ID where saves have to be found
   * @returns {Promise<Save[] | undefined>} - Returns undefined if LocalFolder not found
   */
  /* istanbul ignore next */
  async findAllByFolderId(folderId: number): Promise<Save[] | undefined> {
    const localFolder = await this.localFolderService.findById(folderId);
    if (!localFolder) return undefined;
    const saves = await this.saveRepository.find({ where: { localFolder } });
    return sortSaves(saves);
  }

  findAllExports(): Promise<Save[]> {
    return this.saveRepository.find({
      where: { isExported: true },
      relations: [
        'localFolder',
        'fileMetadatas',
        'fileMetadatas.localFile',
        'packages',
      ],
    });
  }

  async removeNotExportedSaves(localFolder: LocalFolder) {
    await this.saveRepository.delete({ isExported: false, localFolder });
  }
}
