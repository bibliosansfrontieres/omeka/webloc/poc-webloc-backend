import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  NotFoundException,
} from '@nestjs/common';
import { SaveService } from './save.service';
import { CreateSaveDto } from './dto/create-save.dto';

@Controller('save')
export class SaveController {
  constructor(private readonly saveService: SaveService) {}

  @Get('exports')
  async getAllExports() {
    return this.saveService.findAllExports();
  }

  /* istanbul ignore next */
  @Post()
  create(@Body() createSaveDto: CreateSaveDto) {
    return this.saveService.create(createSaveDto);
  }

  /* istanbul ignore next */
  @Get(':id')
  async get(@Param('id') id: string) {
    const save = await this.saveService.findSaveById(+id);
    if (!save) throw new NotFoundException();
    return save;
  }

  /* istanbul ignore next */
  @Get('folder/:folderId')
  getAllFromFromFolderId(@Param('folderId') folderId: string) {
    return this.saveService.findAllByFolderId(+folderId);
  }
}
