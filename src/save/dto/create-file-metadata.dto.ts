import { LocalFile } from '../../local-file/entities/local-file.entity';

export class CreateFileMetadataDto {
  localFile: LocalFile;
  title: string;
  description: string;
  creator: string;
  publisher: string;
  dateCreated: string;
  language: string;
  license: string;
  source: string;
  audience: string;
  collection: string;
  collection2: string;
  subtitles: string;
  duration: string;
  ignore: boolean;
  size: string;
  tag11: string;
  tag12: string;
  tag13: string;
  tag21: string;
  tag22: string;
  tag23: string;
  geography1: string;
  geography2: string;
  temporality1: string;
  temporality2: string;
  package: string;
}
