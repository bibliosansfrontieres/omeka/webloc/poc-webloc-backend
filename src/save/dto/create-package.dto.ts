export class CreatePackageDto {
  id: string;
  index: number;
  name: string;
  description: string;
  goal: string;
  collection: string;
  audience: string;
  language: string;
}
