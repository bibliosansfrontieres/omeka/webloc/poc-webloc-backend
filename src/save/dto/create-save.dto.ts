import { CreateFileMetadataDto } from './create-file-metadata.dto';
import { CreatePackageDto } from './create-package.dto';

export class CreateSaveDto {
  name: string;
  iso: string;
  folderId: number;
  fileMetadatas: CreateFileMetadataDto[];
  packages: CreatePackageDto[];
  email: string | undefined;
}
