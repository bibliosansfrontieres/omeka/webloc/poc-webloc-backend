import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { FileMetadata } from './entities/file-metadata.entity';
import { Save } from './entities/save.entity';
import { SaveController } from './save.controller';
import { SaveService } from './save.service';
import { LocalFolderService } from '../local-folder/local-folder.service';
import { LocalFileService } from '../local-file/local-file.service';

describe('SaveController', () => {
  let controller: SaveController;
  let saveRepository: Repository<Save>;
  let fileMetadataRepository: Repository<FileMetadata>;
  const SAVE_REPOSITORY_TOKEN = getRepositoryToken(Save, 'WebLoc');
  const FILE_METADATA_REPOSITORY_TOKEN = getRepositoryToken(
    FileMetadata,
    'WebLoc',
  );

  const mockLocalFolderService = {};
  const mockLocalFileService = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SaveController],
      providers: [
        SaveService,
        {
          provide: SAVE_REPOSITORY_TOKEN,
          useClass: Repository,
        },
        {
          provide: FILE_METADATA_REPOSITORY_TOKEN,
          useClass: Repository,
        },
        LocalFolderService,
        LocalFileService,
      ],
    })
      .overrideProvider(LocalFolderService)
      .useValue(mockLocalFolderService)
      .overrideProvider(LocalFileService)
      .useValue(mockLocalFileService)
      .compile();

    controller = module.get<SaveController>(SaveController);
    saveRepository = module.get<Repository<Save>>(SAVE_REPOSITORY_TOKEN);
    fileMetadataRepository = module.get<Repository<FileMetadata>>(
      FILE_METADATA_REPOSITORY_TOKEN,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('saveRepository should be defined', () => {
    expect(saveRepository).toBeDefined();
  });

  it('fileMetadataRepository should be defined', () => {
    expect(fileMetadataRepository).toBeDefined();
  });
});
